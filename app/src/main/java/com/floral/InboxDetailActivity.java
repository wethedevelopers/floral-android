package com.floral;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.floral.utils.PreferenceHelper;

public class InboxDetailActivity extends AppCompatActivity {

    TextView tvDescription,tvName,tvBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_detail);


        tvDescription =(TextView) findViewById(R.id.tvDescription);
        tvName=(TextView)findViewById(R.id.tvName);
        tvName.setText(new PreferenceHelper(InboxDetailActivity.this).getName());
        tvBack =(TextView)findViewById(R.id.tvBack);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        Bundle b= getIntent().getExtras();

        if(b!= null)
        {
            String des = b.getString("desc");
            tvDescription.setText(des);
        }

    }

}
