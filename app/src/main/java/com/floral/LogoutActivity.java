package com.floral;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.floral.parse.HttpRequester;
import com.floral.parse.ParseContent;
import com.floral.utils.AndyUtils;
import com.floral.utils.Const;

import java.util.HashMap;

public class LogoutActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflater.inflate(R.layout.activity_logout, flContainer);

        llDeliveryRoute.setSelected(false);
        llDeliveryRoute.setEnabled(true);
        llInbox.setSelected(false);
        llChangePwd.setSelected(false);
        llLogout.setSelected(true);
        //showLogoutDialog();
        logout("");
    }

    private void logout(String note) {
        AndyUtils.showSimpleProgressDialog(LogoutActivity.this);

        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.LOGOUT);
        map.put("id", preferenceHelper.getUserId());
        map.put("Remarks", "");

        new HttpRequester(LogoutActivity.this, map, Const.ServiceCode.LOGOUT, false, this);

    }


    private void showLogoutDialog() {

        final Dialog dialog = new Dialog(this,
                R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(ContextCompat.getColor(this, R.color.color_black_transparent)));
        dialog.setContentView(R.layout.dialog_logout);

        final EditText etNotes = (EditText) dialog.findViewById(R.id.etNotes);

        TextView tvLogout = (TextView) dialog.findViewById(R.id.tvLogout);
        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (TextUtils.isEmpty(etNotes.getText().toString())) {
                    Toast.makeText(LogoutActivity.this, getString(R.string.text_enter_note), Toast.LENGTH_SHORT).show();
                } else {
                    dialog.dismiss();
                    logout(etNotes.getText().toString());
                }

            }
        });
        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();

                Intent intent = new Intent(LogoutActivity.this, DeliveryRouteActivity.class);
                startActivity(intent);
                finish();
            }
        });

        dialog.show();

    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        super.onTaskCompleted(response, serviceCode);

        ParseContent parseContent = new ParseContent(LogoutActivity.this);
        AndyUtils.removeSimpleProgressDialog();
        switch (serviceCode) {
            case Const.ServiceCode.LOGOUT:

                if (parseContent.isStatusOk(response)) {

                    parseContent.isStatusOkOnlyDisplayMsg(response);

                    preferenceHelper.logout();
                    Intent intent = new Intent(LogoutActivity.this, LoginActivity.class);
                    startActivity(intent);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    finish();
                }
        }
    }
}
