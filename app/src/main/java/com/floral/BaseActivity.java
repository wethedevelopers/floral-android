package com.floral;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.floral.parse.AsyncTaskCompleteListener;
import com.floral.utils.PreferenceHelper;
import com.squareup.picasso.Picasso;

public class BaseActivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskCompleteListener {

    //other constants.
    public static final int TAG_MENU = 3;
    public static final int TAG_BACK = 4;
    public ActionBar actionBar;


    public LinearLayout flContainer;
    public LayoutInflater inflater;
    public LinearLayout llDeliveryRoute, llInbox, llChangePwd, llLogout, llMainBasic;
    public PreferenceHelper preferenceHelper;
    private AQuery aQuery;
    private ImageOptions imageOptions;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_base);

        aQuery = new AQuery(BaseActivity.this);
        imageOptions = new ImageOptions();
        imageOptions.memCache = true;
        imageOptions.fileCache = true;
        imageOptions.targetWidth = 200;

        preferenceHelper = new PreferenceHelper(BaseActivity.this);


        inflater = LayoutInflater.from(this);
        flContainer = (LinearLayout) findViewById(R.id.frame);

        llDeliveryRoute = (LinearLayout) findViewById(R.id.llDeliveryRoute);
        llDeliveryRoute.setOnClickListener(this);
        llDeliveryRoute.setSelected(true);

        llInbox = (LinearLayout) findViewById(R.id.llInbox);
        llInbox.setOnClickListener(this);
        llChangePwd = (LinearLayout) findViewById(R.id.llChangePwd);
        llChangePwd.setOnClickListener(this);
        llLogout = (LinearLayout) findViewById(R.id.llLogout);
        llLogout.setOnClickListener(this);

        llMainBasic = (LinearLayout) findViewById(R.id.llMainBasic);


        actionBar = getSupportActionBar();
        assert actionBar != null;

       /* if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            actionBar.setElevation(0);
        }*/

       /* try {
            actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM,
                    ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME
                            | ActionBar.DISPLAY_SHOW_TITLE);
            actionBar.setCustomView(customActionBarView,
                    new ActionBar.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT));

            Toolbar parent = (Toolbar) customActionBarView.getParent();
            parent.setContentInsetsAbsolute(0, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }*/


    }

    public void showPopupImg(View view, String path) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.popup, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        ImageView btnDismiss = (ImageView) popupView.findViewById(R.id.dismiss);
        btnDismiss.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });


        ImageView imgFlower = (ImageView) popupView.findViewById(R.id.imgFlower);


        Picasso.with(BaseActivity.this).load(path).into(imgFlower);
        /*aQuery.id(imgFlower)
                .image(path, imageOptions);*/

        popupWindow.showAsDropDown(view, 0, -0);

    }

    public void showPopupImgHistory(View view, String path, String name, String note) {
        LayoutInflater layoutInflater
                = (LayoutInflater) getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        View popupView = layoutInflater.inflate(R.layout.popup_history, null);
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        ImageView btnDismiss = (ImageView) popupView.findViewById(R.id.dismiss);
        btnDismiss.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        TextView tvNote = (TextView) popupView.findViewById(R.id.tvNote);
        ImageView imgFlower = (ImageView) popupView.findViewById(R.id.imgFlower);
        tvNote.setText(note);
        Picasso.with(BaseActivity.this).load(path).into(imgFlower);
        /*aQuery.id(imgFlower)
                .image(path, imageOptions);*/

        popupWindow.showAsDropDown(view, 0, -0);

    }

    public void onClick(View view) {
        switch (view.getId()) {
           /* case R.id.ivActionMenu:
                //if((int)ivActionMenu.getTag() == TAG_MENU)
                drawerLayout.openDrawer(GravityCompat.START);
                //else
                //  onBackPressed();
                break;*/

            case R.id.llDeliveryRoute:

                Intent intent = new Intent(BaseActivity.this, DeliveryRouteActivity.class);
                startActivity(intent);
                /*llDeliveryRoute.setBackgroundColor(getResources().getColor(R.color.white));
                llInbox.setBackgroundColor(getResources().getColor(R.color.yellow));
                llChangePwd.setBackgroundColor(getResources().getColor(R.color.yellow));
                llLogout.setBackgroundColor(getResources().getColor(R.color.yellow));*/


                break;

            case R.id.llInbox:

                Intent intent3 = new Intent(BaseActivity.this, InboxActivity.class);
                startActivity(intent3);


               /* llDeliveryRoute.setBackgroundColor(getResources().getColor(R.color.yellow));
                llInbox.setBackgroundColor(getResources().getColor(R.color.white));
                llChangePwd.setBackgroundColor(getResources().getColor(R.color.yellow));
                llLogout.setBackgroundColor(getResources().getColor(R.color.yellow));*/

                break;

            case R.id.llChangePwd:

                Intent intent1 = new Intent(BaseActivity.this, ChangePwdActivity.class);
                startActivity(intent1);


               /* llDeliveryRoute.setBackgroundColor(getResources().getColor(R.color.yellow));
                llInbox.setBackgroundColor(getResources().getColor(R.color.yellow));
                llChangePwd.setBackgroundColor(getResources().getColor(R.color.white));
                llLogout.setBackgroundColor(getResources().getColor(R.color.yellow));*/

                break;

            case R.id.llLogout:

                Intent intent2 = new Intent(BaseActivity.this, LogoutActivity.class);
                startActivity(intent2);



                /*llDeliveryRoute.setBackgroundColor(getResources().getColor(R.color.yellow));
                llInbox.setBackgroundColor(getResources().getColor(R.color.yellow));
                llChangePwd.setBackgroundColor(getResources().getColor(R.color.yellow));
                llLogout.setBackgroundColor(getResources().getColor(R.color.white));*/

                break;

        }
    }


    @Override
    public void onTaskCompleted(String str, int i) {

    }
}
