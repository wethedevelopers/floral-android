package com.floral;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class AdvanceSearchActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        inflater.inflate(R.layout.dialog_adavance, flContainer);

        llDeliveryRoute.setSelected(true);
        llDeliveryRoute.setEnabled(false);
        llInbox.setSelected(false);
        llChangePwd.setSelected(false);
        llLogout.setSelected(false);

       /* ListViewFragment fragment = (ListViewFragment) getSupportFragmentManager().findFragmentById(R.id.content_frame);
        fragment.getCutomerAdvanse();*/

        initUI();

    }

    private void initUI()
    {
        TextView tvName = (TextView) findViewById(R.id.tvName);
        final EditText etAddress = (EditText) findViewById(R.id.etAddress);
        final EditText etCity = (EditText) findViewById(R.id.etCity);
        final EditText etZip = (EditText) findViewById(R.id.etZip);
        final TextView tvBack= (TextView) findViewById(R.id.tvBack);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               finish();
            }
        });

        tvName.setText(preferenceHelper.getName());

        TextView tvSearch = (TextView) findViewById(R.id.tvSearch);
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                DeliveryRouteActivity.address = etAddress.getText().toString();
                DeliveryRouteActivity.city = etCity.getText().toString();
                DeliveryRouteActivity. zip = etZip.getText().toString();

               // ListViewFragment fragment = (ListViewFragment) getSupportFragmentManager().findFragmentByTag(Const.FRAGMENT_LIST);
                if(DeliveryRouteActivity.deliveryRouteActivity.listViewFragment != null) {
                    DeliveryRouteActivity.deliveryRouteActivity.listViewFragment.getCutomerAdvanse(etAddress.getText().toString(), etCity.getText().toString(), etZip.getText().toString());
                }else
                {
                    DeliveryRouteActivity.deliveryRouteActivity.mapViewFragment.getCutomerAdvanse(etAddress.getText().toString(), etCity.getText().toString(), etZip.getText().toString());
                }

                finish();

            }
        });

        TextView tvCancel = (TextView) findViewById(R.id.tvCancel);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

}
