package com.floral.component;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckBox;

/**
 * Created by bhagvati on 1/2/17.
 */
public class CheckBoxRegular extends CheckBox {

    private Typeface typeface;

    public CheckBoxRegular(Context context) {
        super(context);
    }

    public CheckBoxRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context);
    }

    public CheckBoxRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context);
    }

    private boolean setCustomFont(Context ctx) {
        try {
            if (this.typeface == null) {
                this.typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/titilliumweb_semi_bold.ttf");
            }
            setTypeface(this.typeface);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
