package com.floral.component;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.CheckedTextView;

/**
 * Created by bhagvati on 1/2/17.
 */
public class CheckedTextViewRegular extends CheckedTextView {
    private Typeface typeface;

    public CheckedTextViewRegular(Context context) {
        super(context);
    }

    public CheckedTextViewRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context);
    }

    public CheckedTextViewRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context);
    }

    private boolean setCustomFont(Context ctx) {
        try {
            if (this.typeface == null) {
                this.typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/titilliumweb_semi_bold.ttf");
            }
            setTypeface(this.typeface);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}

