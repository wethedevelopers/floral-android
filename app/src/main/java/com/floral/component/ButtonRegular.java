package com.floral.component;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class ButtonRegular extends Button {
    private Typeface typeface;

    public ButtonRegular(Context context) {
        super(context);
    }

    public ButtonRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context);
    }

    public ButtonRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context);
    }

    private boolean setCustomFont(Context ctx) {
        try {
            if (this.typeface == null) {
                this.typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/titilliumweb_semi_bold.ttf");
            }
            setTypeface(this.typeface);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
