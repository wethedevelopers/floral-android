package com.floral.component;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class TextViewBold extends android.support.v7.widget.AppCompatTextView {
    private Typeface typeface;

    public TextViewBold(Context context) {
        super(context);
    }

    public TextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        setCustomFont(context);
    }

    public TextViewBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setCustomFont(context);
    }

    private boolean setCustomFont(Context ctx) {
        try {
            if (this.typeface == null) {
                this.typeface = Typeface.createFromAsset(ctx.getAssets(), "fonts/titillium_web_bold.ttf");
            }
            setTypeface(this.typeface);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
