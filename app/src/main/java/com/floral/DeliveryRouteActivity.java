package com.floral;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.floral.adapter.DrawerAdapter;
import com.floral.fragment.BaseFragment;
import com.floral.fragment.ListViewFragment;
import com.floral.fragment.MapViewFragment;
import com.floral.model.Customer;
import com.floral.utils.Const;

import java.util.ArrayList;
import java.util.List;

public class DeliveryRouteActivity extends BaseActivity implements View.OnClickListener {

    public static DeliveryRouteActivity deliveryRouteActivity;
    public static String address = "", city = "", zip = "";
    public ImageView ivActionMenu;
    public DrawerLayout drawerLayout;
    public List<Customer> listRoute;
    public List<Customer> listRouteTemp;
    public String zoneId, zoneName;
    public EditText etSearch;
    public TextView tvAddress, tvClose, tvAdvance;
    // private ArrayList<Zone> zoneList;
    public Button btnFilter;
    public ListViewFragment listViewFragment;
    public MapViewFragment mapViewFragment;
    public int selectedPosition = -1;
    private Button btnListView, btnMapView;
    private FrameLayout content_frame;
    private FragmentManager manager;
    private DrawerLayout drawer;
    private ListView listDrawer;
    private CheckedTextView tvZone;
    private boolean toggle = true;
    private TextView tvUserName;
    private BaseFragment fragment;
    private DrawerAdapter adapter;
    private AlertDialog locationAlertDialog;
    private Typeface externalFont, externalFont1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        deliveryRouteActivity = this;

        // setContentView(R.layout.activity_delivery_route);
        inflater.inflate(R.layout.activity_delivery_route, flContainer);

        llDeliveryRoute.setSelected(true);
        llDeliveryRoute.setEnabled(false);
        llInbox.setSelected(false);
        llChangePwd.setSelected(false);
        llLogout.setSelected(false);

        initUI();
        moveDrawerToTop();
        initialiseDrawer();
        loadLisviewFragment();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // zoneList=Const.ZONE_LIST;


    }


    private void initUI() {

        listRoute = new ArrayList();
        listRouteTemp = new ArrayList();


        Bundle b = getIntent().getExtras();

        if (b != null) {
            zoneId = b.getString("zoneId");
            zoneName = b.getString("zoneName");

            Const.ZONE_ID = zoneId;
            Const.ZONE_NAME = zoneName;
        }

        content_frame = (FrameLayout) findViewById(R.id.
                content_frame);
        manager = getSupportFragmentManager();

        fragment = new ListViewFragment();

        btnListView = (Button) findViewById(R.id.btnListView);
        btnListView.setOnClickListener(this);
        btnListView.setSelected(true);
        btnMapView = (Button) findViewById(R.id.btnMapView);
        btnMapView.setOnClickListener(this);

        externalFont = Typeface.createFromAsset(getAssets(), "fonts/titillium_web_bold.ttf");
        btnListView.setTypeface(externalFont);

        externalFont1 = Typeface.createFromAsset(getAssets(), "fonts/titilliumweb.ttf");
        btnMapView.setTypeface(externalFont1);


        etSearch = (EditText) findViewById(R.id.etSearch);
        tvClose = (TextView) findViewById(R.id.tvClose);

       /* etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {


                    if(!TextUtils.isEmpty(etSearch.getText().toString()))
                    fragment.searchCustomer(DeliveryRouteActivity.this,etSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });*/


        ivActionMenu = (ImageView) findViewById(R.id.ivActionMenu);
        ivActionMenu.setOnClickListener(this);

        tvAdvance = (TextView) findViewById(R.id.tvAdvance);
        // tvAdvance.setOnClickListener(this);

        tvAddress = (TextView) findViewById(R.id.tvAddress);
        tvAddress.setText("Zone-" + Const.ZONE_NAME + "," + "ALL");
        Const.ADDRESS = tvAddress.getText().toString();

        tvUserName = (TextView) findViewById(R.id.tvUserName);
        tvUserName.setText(preferenceHelper.getName());
    }

    public void loadLisviewFragment() {
        listViewFragment = new ListViewFragment();
        manager.beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                R.anim.slide_in_left, R.anim.slide_out_right).replace(R.id.content_frame, listViewFragment, Const.FRAGMENT_LIST).commit();
    }

    public void loadMapViewFragment() {
        mapViewFragment = new MapViewFragment();
        manager.beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                R.anim.slide_in_left, R.anim.slide_out_right).replace(R.id.content_frame, mapViewFragment, Const.FRAGMENT_MAP).commit();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()) {
            case R.id.btnListView:

                btnListView.setTypeface(externalFont);

                btnMapView.setTypeface(externalFont1);


                btnListView.setSelected(true);
                btnMapView.setSelected(false);
                fragment = new ListViewFragment();
                loadLisviewFragment();


                break;

            case R.id.btnMapView:

                btnMapView.setTypeface(externalFont);
                btnListView.setTypeface(externalFont1);

                btnListView.setSelected(false);
                btnMapView.setSelected(true);
                fragment = new MapViewFragment();
                loadMapViewFragment();

                break;

            case R.id.ivActionMenu:
                //if((int)ivActionMenu.getTag() == TAG_MENU)
                drawerLayout.openDrawer(GravityCompat.END);


                //else
                //  onBackPressed();
                break;

            // case R.id.tvAdvance:

            // showAdvanceDialog();

            //       break;
        }
    }


    public void myClickMethod(View v) {
        fragment.myClickMethod(v);
    }

    protected void moveDrawerToTop() {
        drawer = (DrawerLayout) ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.activity_main, null);
        ViewGroup decor = (ViewGroup) getWindow().getDecorView();
        View child = decor.getChildAt(0);
        decor.removeView(child);
        ((LinearLayout) this.drawer.findViewById(R.id.llContent)).addView(child, 0);
        drawer.findViewById(R.id.layoutDrawer).setPadding(0, 40 + getStatusBarHeight(), 0, 0);
        decor.addView(this.drawer);
    }

    protected int getStatusBarHeight() {
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return getResources().getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    protected void initialiseDrawer() {
        drawerLayout = (DrawerLayout) drawer.findViewById(R.id.layout_drawer);
        listDrawer = (ListView) drawer.findViewById(R.id.left_drawer);
        listDrawer.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        tvZone = (CheckedTextView) drawer.findViewById(R.id.tvZone);

        RadioGroup radioGroupFilter = (RadioGroup) drawer.findViewById(R.id.radioGroupFilter);
        RadioButton cbChangesDone = (RadioButton) drawer.findViewById(R.id.cbChangesDone);
        RadioButton cbChangesPending = (RadioButton) drawer.findViewById(R.id.cbChangesPending);
        RadioButton cbDue = (RadioButton) drawer.findViewById(R.id.cbDue);
        RadioButton cbAll = (RadioButton) drawer.findViewById(R.id.cbAll);

        radioGroupFilter.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.cbChangesDone:
                        // do operations specific to this selection
                        Const.FILTER_TYPE = Const.GREEN;

                        break;
                    case R.id.cbChangesPending:
                        // do operations specific to this selection
                        Const.FILTER_TYPE = Const.YELLOW;

                        break;
                    case R.id.cbDue:
                        // do operations specific to this selection
                        Const.FILTER_TYPE = Const.RED;

                        break;

                    case R.id.cbAll:

                        Const.FILTER_TYPE = Const.ALL;

                        break;
                }

            }
        });


        btnFilter = (Button) drawer.findViewById(R.id.btnFilter);
       /* btnFilter.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

            }
        });*/


        tvZone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (toggle) {
                    toggle = false;
                    listDrawer.setVisibility(View.INVISIBLE);
                    tvZone.setCheckMarkDrawable(getResources().getDrawable(R.drawable.plus_checktv));

                    /*listDrawer.animate()
                            .translationY(view.getHeight())
                            .alpha(0.0f)
                            .setDuration(500).setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            listDrawer.setVisibility(View.INVISIBLE);
                        }
                    });*/
                } else {

                    listDrawer.setVisibility(View.VISIBLE);
                    tvZone.setCheckMarkDrawable(getResources().getDrawable(R.drawable.minus_checktv));

                    toggle = true;


                }
            }
        });


        for (int i = 0; i < Const.ZONE_LIST.size(); i++) {
            if (Const.ZONE_LIST.get(i).isSelected()) {
                selectedPosition = i;
            }
        }


        adapter = new DrawerAdapter(this, Const.ZONE_LIST, selectedPosition);

        listDrawer.setAdapter(adapter);


    }

    public void changeStateCheck(int pos) {
        for (int i = 0; i < Const.ZONE_LIST.size(); i++) {
            if (Const.ZONE_LIST.get(i).isSelected()) {
                Const.ZONE_LIST.get(i).setSelected(true);
            } else {
                Const.ZONE_LIST.get(i).setSelected(false);
            }
        }

        adapter.notifyDataSetChanged();
    }

    public void showLocationOffDialog() {

        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(
                DeliveryRouteActivity.this);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle(getString(R.string.dialog_no_location_service_title))
                .setMessage(getString(R.string.dialog_no_location_service))
                .setPositiveButton(
                        getString(R.string.dialog_enable_location_service),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // continue with delete
                                dialog.dismiss();
                                Intent viewIntent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(viewIntent);

                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                // do nothing
                                dialog.dismiss();
                                finish();
                            }
                        });
        locationAlertDialog = gpsBuilder.create();
        locationAlertDialog.show();
    }


}

