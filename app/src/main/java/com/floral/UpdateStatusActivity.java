package com.floral;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.floral.adapter.UpdateStatusGridAdapter;
import com.floral.component.MarginDecoration;
import com.floral.model.Customer;
import com.floral.parse.HttpRequester;
import com.floral.parse.ParseContent;
import com.floral.utils.AndyUtils;
import com.floral.utils.Const;
import com.floral.utils.ImageHelper;
import com.soundcloud.android.crop.Crop;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;

public class UpdateStatusActivity extends BaseActivity
{

    private Button btnYes,btnNo;
    private TextView tvBack,tvClear,tvUpdate,tvName,tvSubmit;
    private LinearLayout llYEs,llNo;
    private EditText etNote,etNoteYes;
    private Customer data;
    private String note="";
   // private String imgArray[];
    private RecyclerView recyclerView;
    private UpdateStatusGridAdapter gridAdpt;
    private AQuery aQuery;

    private String profileImageFilePath, filePath = "", profileImageData;
    private int rotationAngle;
    private Bitmap photoBitmap;
    private Uri uri = null;
    private ImageOptions imageOptions;
    public TextView tvAddress;
    private int pos;
    private Typeface externalFont,externalFont1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        inflater.inflate(R.layout.activity_update_status, flContainer);

        initUI();
    }

    private void initUI()
    {
        Bundle b= getIntent().getExtras();

        data = (Customer) b.getSerializable("data");

        btnYes= (Button) findViewById(R.id.btnYes);
        btnYes.setOnClickListener(this);
        btnNo= (Button) findViewById(R.id.btnNo);
        btnNo.setOnClickListener(this);
        btnYes.setSelected(true);

        externalFont= Typeface.createFromAsset(getAssets(), "fonts/titillium_web_bold.ttf");
        btnYes.setTypeface(externalFont);

        externalFont1 =Typeface.createFromAsset(getAssets(), "fonts/titilliumweb.ttf");
        btnNo.setTypeface(externalFont1);


        aQuery = new AQuery(UpdateStatusActivity.this);
        imageOptions = new ImageOptions();
        imageOptions.memCache = true;
        imageOptions.fileCache = true;
        imageOptions.targetWidth = 200;

       final GestureDetector mGestureDetector = new GestureDetector(UpdateStatusActivity.this,
                new GestureDetector.SimpleOnGestureListener() {

                    @Override
                    public boolean onSingleTapUp(MotionEvent e) {
                        return true;
                    }

                });



        tvBack = (TextView) findViewById(R.id.tvBack);
        tvBack.setOnClickListener(this);
        tvClear =(TextView) findViewById(R.id.tvClear);
        tvClear.setOnClickListener(this);
        tvUpdate = (TextView) findViewById(R.id.tvUpdate);
        tvUpdate.setOnClickListener(this);
        tvName =(TextView) findViewById(R.id.tvName);
        tvAddress =(TextView)findViewById(R.id.tvAddress);


        etNote =(EditText)findViewById(R.id.etNote);
        etNoteYes =(EditText) findViewById(R.id.etNoteYes);

        llYEs =(LinearLayout) findViewById(R.id.llYEs);
        llNo = (LinearLayout) findViewById(R.id.llNo);

        tvName.setText(data.getCustomerName());
        tvAddress.setText(data.getAddress());
        etNoteYes.setText(data.getNotes());


        Log.d("get change over :: ",data.getChangeover_image()+"");

        /*imgArray=data.getChangeover_image().split(",");*/



       // Log.d("img array size :: ",imgArray.length+"");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        gridAdpt = new UpdateStatusGridAdapter(UpdateStatusActivity.this, data.changeOverImg);

        recyclerView.addItemDecoration(new MarginDecoration(3, 10, true, 0));
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(UpdateStatusActivity.this, 3);
        gridLayoutManager.supportsPredictiveItemAnimations();
        recyclerView.setLayoutManager(gridLayoutManager);


        recyclerView.getRecycledViewPool().clear();
        //recyclerViewState = recyclerView.getLayoutManager().onSaveInstanceState();
        recyclerView.setAdapter(gridAdpt);

       /* recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {


            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                pos = recyclerView.getChildPosition(child);

                if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {

                   showPopupImg(tvAddress, data.changeOverImg[pos]);

                    return true;

                }

                return false;
            }


            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });*/

        tvSubmit =(TextView) findViewById(R.id.tvSubmit);
        tvSubmit.setOnClickListener(this);


    }


    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Const.PERMISSION_STORAGE_REQUEST_CODE:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        gridAdpt.choosePhotoFromGallary();
                    }
                }
                break;
            case Const.PERMISSION_CAMERA_REQUEST_CODE:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        gridAdpt.takePhotoFromCamera();
                    }
                }
                break;
        }

    }
    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId())
        {
            case R.id.btnYes :

                btnYes.setSelected(true);
                btnNo.setSelected(false);
                llYEs.setVisibility(View.VISIBLE);
                llNo.setVisibility(View.GONE);

                btnYes.setTypeface(externalFont);

                btnNo.setTypeface(externalFont1);


                break;

            case R.id.btnNo :

                btnYes.setSelected(false);
                btnNo.setSelected(true);
                llYEs.setVisibility(View.GONE);
                llNo.setVisibility(View.VISIBLE);

                btnYes.setTypeface(externalFont1);

                btnNo.setTypeface(externalFont);

                break;

            case R.id.tvClear :


                etNoteYes.setText("");

                break;

            case R.id.tvUpdate :

                note =etNoteYes.getText().toString();

                if(note.contains("&"))
                {
                    note = note.replace("&","*^*");
                }
                changeOverUpdate();

                break;

            case R.id.tvBack:

                finish();

                break;

            case R.id.tvSubmit :

            if(!TextUtils.isEmpty(etNote.getText().toString()))
            {
                  updateNoteStausNo();
            }

            break;
        }
    }

    private void changeOverUpdate()
    {

        if (!AndyUtils.isNetworkAvailable(UpdateStatusActivity.this)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    UpdateStatusActivity.this);
            return;
        }

            AndyUtils.showSimpleProgressDialog(UpdateStatusActivity.this);

            HashMap<String, String> map = new HashMap<>();
            map.put(Const.URL, Const.ServiceType.CHANGE_OVER_REMARK_YES);

            map.put("id", preferenceHelper.getUserId());
            map.put("customerID", data.getID());
            map.put("Remarks_yes", note);

            new HttpRequester(UpdateStatusActivity.this, map, Const.ServiceCode.CHANGE_OVER_REMARK_YES, false, this);
    }


    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        super.onTaskCompleted(response, serviceCode);

        ParseContent parseContent = new ParseContent(UpdateStatusActivity.this);
        AndyUtils.removeSimpleProgressDialog();

        switch (serviceCode) {
            case Const.ServiceCode.CHANGE_OVER_REMARK_YES:
                if (parseContent.isStatusOkWithToast(response)) {

                }
                break;
            case Const.ServiceCode.CHANGE_OVER_IMGE:
                if (parseContent.isStatusOkWithToast(response)) {
                    String path = parseContent.getUploadImage(response);
                    if(path != null) {
                        data.changeOverImg[Const.imgPosition] = path;
                        gridAdpt.notifyDataSetChanged();
                    }
                }
                break;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d("OnActivt REsult ::::  ","okookokoo "+ resultCode+" "+requestCode);


        switch (requestCode) {

            case  Const.CHOOSE_PHOTO :
                if (data != null) {
                    Uri uri = data.getData();

                    Log.d("URI :: ",uri.toString());

                    profileImageFilePath = getRealPathFromURI(uri);


                    filePath = profileImageFilePath;

                    if (filePath != null) {
                        try {
                            int mobile_width = 480;
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            BitmapFactory.decodeFile(filePath, options);
                            int outWidth = options.outWidth;
                            int ratio = (int) ((((float) outWidth) / mobile_width) + 0.5f);

                            if (ratio == 0) {
                                ratio = 1;
                            }
                            ExifInterface exif = new ExifInterface(filePath);

                            String orientString = exif
                                    .getAttribute(ExifInterface.TAG_ORIENTATION);
                            int orientation = orientString != null ? Integer
                                    .parseInt(orientString)
                                    : ExifInterface.ORIENTATION_NORMAL;

                            if (orientation == ExifInterface.ORIENTATION_ROTATE_90)
                                rotationAngle = 90;
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
                                rotationAngle = 180;
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
                                rotationAngle = 270;

                            System.out.println("Rotation : " + rotationAngle);

                            options.inJustDecodeBounds = false;
                            options.inSampleSize = ratio;

                            photoBitmap = BitmapFactory.decodeFile(filePath, options);
                            if (photoBitmap != null) {
                                Matrix matrix = new Matrix();
                                matrix.setRotate(rotationAngle,
                                        (float) photoBitmap.getWidth() / 2,
                                        (float) photoBitmap.getHeight() / 2);
                                photoBitmap = Bitmap.createBitmap(photoBitmap, 0, 0,
                                        photoBitmap.getWidth(),
                                        photoBitmap.getHeight(), matrix, true);

                                String path = MediaStore.Images.Media.insertImage(
                                        getContentResolver(), photoBitmap, Calendar
                                                .getInstance().getTimeInMillis()
                                                + ".jpg", null);

                                Log.d("string path :: ",""+path);

                                if (!TextUtils.isEmpty(path)) {
                                    beginCrop(Uri.parse(path));
                                } else {
                                    AndyUtils
                                            .showToast(
                                                    getResources()
                                                            .getString(
                                                                    R.string.text_unable_to_select_image),
                                                    UpdateStatusActivity.this);
                                }

                            }
                        } catch (OutOfMemoryError e) {
                            System.out.println("out of bound");
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    Toast.makeText(
                            this,
                            getResources().getString(
                                    R.string.text_unable_to_select_image),
                            Toast.LENGTH_LONG).show();
                }

                break;


            case Const.TAKE_PHOTO :

                if (Const.cameraURI != null) {

                    String imageFilePath = Const.cameraURI.getPath();


                    Log.d("image file path  :: ", "" + imageFilePath);

                    if (imageFilePath != null && imageFilePath.length() > 0) {
                        try {
                            int mobile_width = 480;
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inJustDecodeBounds = true;
                            int outWidth = options.outWidth;
                            int outHeight = options.outHeight;
                            int ratio = (int) ((((float) outWidth) / mobile_width) + 0.5f);

                            if (ratio == 0)
                            {
                                ratio = 1;
                            }

                            ExifInterface exif = new ExifInterface(imageFilePath);

                            String orientString = exif
                                    .getAttribute(ExifInterface.TAG_ORIENTATION);
                            int orientation = orientString != null ? Integer
                                    .parseInt(orientString)
                                    : ExifInterface.ORIENTATION_NORMAL;
                            System.out.println("Orientation : " + orientation);
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_90)
                                rotationAngle = 90;
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_180)
                                rotationAngle = 180;
                            if (orientation == ExifInterface.ORIENTATION_ROTATE_270)
                                rotationAngle = 270;
                            if (orientation == ExifInterface.ORIENTATION_UNDEFINED)
                                rotationAngle = 0;
                            if (orientation == ExifInterface.ORIENTATION_TRANSVERSE)
                                rotationAngle = -90;
                            if (orientation == ExifInterface.ORIENTATION_TRANSPOSE)
                                rotationAngle = 90;
                            if (orientation == ExifInterface.ORIENTATION_FLIP_VERTICAL)
                                rotationAngle = 180;

                            System.out.println("Rotation : " + rotationAngle);

                            options.inJustDecodeBounds = false;
                            options.inSampleSize = ratio;


                            File myFile = new File(imageFilePath);
                            Bitmap bmp = new ImageHelper().decodeFile(myFile);

                            Matrix matrix = new Matrix();
                            matrix.setRotate(rotationAngle,
                                    (float) bmp.getWidth() / 2,
                                    (float) bmp.getHeight() / 2);

                            bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(),
                                    bmp.getHeight(), matrix, true);


                            FileOutputStream outStream = new FileOutputStream(
                                    myFile);

                            bmp.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
                            outStream.flush();
                            outStream.close();

                            String path = MediaStore.Images.Media.insertImage(
                                    getContentResolver(), bmp,
                                    Calendar.getInstance().getTimeInMillis()
                                            + ".jpg", null);



                            Log.d("file path BTMP :: ",path);

                          /*  String imagepath =getRealPathFromURI(uri);
                            final int THUMBSIZE = 64;
                            Bitmap ThumbImage = ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(imagepath),
                                    THUMBSIZE, THUMBSIZE);
                            imageViewItem.setImageBitmap(ThumbImage);*/

                            if (!TextUtils.isEmpty(path))
                            {
                                beginCrop(Uri.parse(path));
                            }

                        } catch (OutOfMemoryError e)
                        {
                            System.out.println("out of bound");
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                    }
                } else {
                    Toast.makeText(
                            this,
                            getResources().getString(
                                    R.string.text_unable_to_select_image),
                            Toast.LENGTH_LONG).show();
                }


                break;
            case Crop.REQUEST_CROP :

                handleCrop(resultCode, data);
                break;


        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {

        if (inImage == null || inContext == null)
        {
            Log.d("img null ::: ", "okokok");
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null,
                null, null);

        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            result = contentURI.getPath();

            Log.d("result :: ",""+result);
        } else {
            cursor.moveToFirst();
            try {
                int idx = cursor
                        .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                result = cursor.getString(idx);
            } catch (Exception e) {
                AndyUtils
                        .showToast(
                                getResources().getString(
                                        R.string.text_unable_to_select_image), this);
                result = "";
            }

            cursor.close();
        }
        return result;
    }


    private void beginCrop(Uri source) {
        // Uri outputUri = Uri.fromFile(new File(registerActivity.getCacheDir(),
        // "cropped"));

        Log.d("SOurce :::::: ",""+source);

        Uri outputUri = Uri.fromFile(new File(Environment
                .getExternalStorageDirectory(), (Calendar.getInstance()
                .getTimeInMillis() + ".jpg")));
        new Crop(source).output(outputUri).asSquare().start(UpdateStatusActivity.this);
    }

    private void handleCrop(int resultCode, Intent result)
    {
        if (resultCode == RESULT_OK)
        {

            profileImageData = getRealPathFromURI(Crop.getOutput(result));
            filePath = getRealPathFromURI(Crop.getOutput(result));

            Log.d("File path herr ------> ",filePath);
           /* data.changeOverImg[Const.imgPosition]=filePath;
            gridAdpt.notifyDataSetChanged();*/


            uploadImage(Const.imgPosition,filePath);

           // gridAdpt.setImage(Crop.getOutput(result));

        } else if (resultCode == Crop.RESULT_ERROR)
        {
            Toast.makeText(this, Crop.getError(result).getMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }


    private void updateNoteStausNo()
    {
        note =etNote.getText().toString();

        if(note.contains("&"))
        {
            note = note.replace("&","*^*");
        }

        AndyUtils.showSimpleProgressDialog(UpdateStatusActivity.this);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.CHANGE_OVER_IMGE);

        map.put("id", preferenceHelper.getUserId());
        map.put("customerID", data.getID());
        map.put("Remarks",note);
        map.put("ChangeOverStatus","no");

        new HttpRequester(UpdateStatusActivity.this, map, Const.ServiceCode.CHANGE_OVER_IMGE, false, this);
    }

    private void uploadImage(int pos, String filepath)
    {

        note =etNoteYes.getText().toString();

        if(note.contains("&"))
        {
            note = note.replace("&","*^*");
        }

        AndyUtils.showSimpleProgressDialog(UpdateStatusActivity.this);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.CHANGE_OVER_IMGE);

        map.put("id", preferenceHelper.getUserId());
        map.put("customerID", data.getID());
        map.put("ChangeOverStatus","yes");
        map.put("FlowerOrder", (pos)+"");
        map.put("Remarks_yes",note);
        map.put("changeOver_Image", filepath);

        new HttpRequester(UpdateStatusActivity.this, map, Const.ServiceCode.CHANGE_OVER_IMGE, false, this);
    }




}
