package com.floral.model;

import java.io.Serializable;

/**
 * Created by bhagvati on 31/1/17.
 */
public class ListRoute implements Serializable
{
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
