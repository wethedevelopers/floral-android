package com.floral.model;

import java.io.Serializable;

/**
 * Created by bhagvati on 30/1/17.
 */
public class Zone implements Serializable
{

    private String ID;
    private String ZoneName;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    private boolean isSelected;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getZoneName() {
        return ZoneName;
    }

    public void setZoneName(String zoneName) {
        ZoneName = zoneName;
    }



}
