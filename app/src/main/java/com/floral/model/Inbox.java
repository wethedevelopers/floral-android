package com.floral.model;

import java.io.Serializable;

/**
 * Created by bhagvati on 9/2/17.
 */
public class Inbox implements Serializable {

    private String ID;
    private String Message;
    private String IsRead;
    private String Sender;
    private String SentOn;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getIsRead() {
        return IsRead;
    }

    public void setIsRead(String isRead) {
        IsRead = isRead;
    }

    public String getSender() {
        return Sender;
    }

    public void setSender(String sender) {
        Sender = sender;
    }

    public String getSentOn() {
        return SentOn;
    }

    public void setSentOn(String sentOn) {
        SentOn = sentOn;
    }



}
