package com.floral.model;

import java.io.Serializable;

/**
 * Created by bhagvati on 3/2/17.
 */
public class Customer implements Serializable {

    public String[] changeOverImg = new String[0];
    public String Latitude;
    public String Longitude;
    public int dupliate_flage = 0;
    private String ID;
    private String CustomerName;
    private String TotalUnits;
    private String ChangeOverPinStatus;
    private String Address;
    private String ContactNumber;
    private String Notes;
    private String Email;
    private String Stage;
    private String Products;
    private String ZoneID;
    private String ChangeOverDate;
    private int total_customers;
    private int previousYear;
    private int previousMonth;
    private int next_year;
    private int next_month;
    private int completed_customers;
    private String changeover_image;

    public int getNext_month() {
        return next_month;
    }

    public void setNext_month(int next_month) {
        this.next_month = next_month;
    }

    public int getNext_year() {
        return next_year;
    }

    public void setNext_year(int next_year) {
        this.next_year = next_year;
    }

    public int getPreviousYear() {
        return previousYear;
    }

    public void setPreviousYear(int previousYear) {
        this.previousYear = previousYear;
    }

    public int getPreviousMonth() {
        return previousMonth;
    }

    public void setPreviousMonth(int previousMonth) {
        this.previousMonth = previousMonth;
    }

    public int getCompleted_customers() {
        return completed_customers;
    }

    public void setCompleted_customers(int completed_customers) {
        this.completed_customers = completed_customers;
    }

    public int getTotal_customers() {
        return total_customers;
    }

    public void setTotal_customers(int total_customers) {
        this.total_customers = total_customers;
    }

    public String getChangeover_image() {
        return changeover_image;
    }

    public void setChangeover_image(String changeover_image) {
        this.changeover_image = changeover_image;
    }

    public String getChangeOverDate() {
        return ChangeOverDate;
    }

    public void setChangeOverDate(String changeOverDate) {
        ChangeOverDate = changeOverDate;
    }

    public String getZoneID() {
        return ZoneID;
    }

    public void setZoneID(String zoneID) {
        ZoneID = zoneID;
    }

    public String getProducts() {
        return Products;
    }

    public void setProducts(String products) {
        Products = products;
    }

    public String getStage() {
        return Stage;
    }

    public void setStage(String stage) {
        Stage = stage;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getContactNumber() {
        return ContactNumber;
    }

    public void setContactNumber(String contactNumber) {
        ContactNumber = contactNumber;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getChangeOverPinStatus() {
        return ChangeOverPinStatus;
    }

    public void setChangeOverPinStatus(String changeOverPinStatus) {
        ChangeOverPinStatus = changeOverPinStatus;
    }

    public String getTotalUnits() {
        return TotalUnits;
    }

    public void setTotalUnits(String totalUnits) {
        TotalUnits = totalUnits;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }


}
