package com.floral.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.floral.App;
import com.floral.DeliveryRouteActivity;
import com.floral.DetailActivity;
import com.floral.R;
import com.floral.location.LocationHelper;
import com.floral.model.BeanRoute;
import com.floral.model.BeanStep;
import com.floral.model.Customer;
import com.floral.model.Route;
import com.floral.parse.HttpRequester;
import com.floral.parse.ParseContent;
import com.floral.utils.AndyUtils;
import com.floral.utils.Const;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MapViewFragment extends BaseFragment implements OnMapReadyCallback
        , LocationHelper.OnLocationReceived {
    static final float COORDINATE_OFFSET = 0.00002f;
    private View view;
    private SupportMapFragment mapFragment;
    private GoogleMap map;
    private boolean isGpsDialogShowing;
    private Address address;
    private Location myLocation;
    private LocationHelper locHelper;
    private AlertDialog gpsAlertDialog;
    public BroadcastReceiver GpsChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final LocationManager manager = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                removeLocationOffDialog();
            } else {
                if (isGpsDialogShowing) {
                    return;
                }
                showLocationOffDialog();
            }
        }
    };
    private ArrayList<LatLng> pointsClient;
    private PolylineOptions lineOptionsClient;
    private Polyline polyLineClient;
    private Route route;
    private ArrayList<LatLng> points;
    private PolylineOptions lineOptions;
    private Polyline polyLine;
    private Marker markerDestination, markerSource;
    private int position = 0;
    private HashMap<Marker, Customer> mapMarker;
    private String strAddress = null;
    private TextView tvMyLocation, tvNextLoction, tvDistane, tvNavigation;
    private double firstLat = 0.0, firstLong = 0.0, secondLat = 0.0, secondLong = 0.0, destLat = 0.0, destLong = 0.0;
    private boolean isLocationClick = false;
    private BeanRoute routeDest, routeClient;
    private ArrayList<LatLng> pointsDest;
    private PolylineOptions lineOptionsDest;
    private Polyline polyLineDest;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_map_view, container, false);

        mapMarker = new HashMap<Marker, Customer>();

        tvMyLocation = (TextView) view.findViewById(R.id.tvMyLocation);
        tvNextLoction = (TextView) view.findViewById(R.id.tvNextLoction);
        tvDistane = (TextView) view.findViewById(R.id.tvDistane);

        initUI();
        initMAP();
        setUpMapIfNeeded();

        return view;
    }

    private void initUI() {


        tvNavigation = (TextView) view.findViewById(R.id.tvNavigation);
        tvNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                isLocationClick = true;


                LocationManager manager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    showLocationOffDialog();
                } else {
                    removeLocationOffDialog();
                }

                activity.registerReceiver(GpsChangeReceiver, new IntentFilter(
                        LocationManager.PROVIDERS_CHANGED_ACTION));

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    checkLocationPermission();
                } else {
                    locationHelper();

                   /* String url = Const.DIRECTION_API + "saddr=" + String.valueOf(myLocation.getLatitude()) + ","  + String.valueOf(myLocation.getLongitude()) + "&" + "daddr=" + event.getLatitude() + "," + event.getLongitude();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(browserIntent);*/

                }

            }
        });


        ((DeliveryRouteActivity) activity).etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager inputMethodManager = (InputMethodManager) activity
                            .getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus()
                            .getWindowToken(), 0);

                    if (!TextUtils.isEmpty(((DeliveryRouteActivity) activity).etSearch.getText().toString()))
                        searchCustomer(((DeliveryRouteActivity) activity).etSearch.getText().toString());
                    return true;
                }

                return false;
            }
        });


        ((DeliveryRouteActivity) activity).etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0) {
                    ((DeliveryRouteActivity) activity).tvClose.setVisibility(View.VISIBLE);
                }

            }
        });

        ((DeliveryRouteActivity) activity).tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DeliveryRouteActivity) activity).etSearch.setText("");
                getCustomer();
                ((DeliveryRouteActivity) activity).tvClose.setVisibility(View.GONE);

            }
        });

        ((DeliveryRouteActivity) activity).tvAdvance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAdvanceDialog();
            }
        });


        ((DeliveryRouteActivity) activity).btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DeliveryRouteActivity) activity).drawerLayout.closeDrawer(Gravity.RIGHT);
                getFilters();
            }
        });

    }

    private void locationHelper() {
        locHelper = new LocationHelper(activity);
        locHelper.setLocationReceivedLister(this);
        locHelper.onStart();
    }

    private void getFilters() {
        if (!AndyUtils.isNetworkAvailable(activity)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    activity);
            return;
        }

        AndyUtils.showSimpleProgressDialog(activity);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.SEARCH);

        map.put("id", pHelper.getUserId());
        map.put("zone_id", Const.ZONE_ID);
        map.put("type_fillter", Const.FILTER_TYPE);

        new HttpRequester(activity, map, Const.ServiceCode.GET_CUSTOMER, false, this);
    }

    private void getCustomer() {
        if (!AndyUtils.isNetworkAvailable(activity)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    activity);
            return;
        }
        AndyUtils.showSimpleProgressDialog(activity);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.GET_CUSTOMER);
        map.put("id", pHelper.getUserId());
        map.put("zone_id", Const.ZONE_ID);
        map.put("type_fillter", Const.FILTER_TYPE);

        new HttpRequester(activity, map, Const.ServiceCode.GET_CUSTOMER, false, this);
    }

    public void searchCustomer(String str) {
        AndyUtils.showSimpleProgressDialog(activity);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.SEARCH);
        map.put("id", pHelper.getUserId());
        map.put("zone_id", Const.ZONE_ID);
        map.put("type_fillter", Const.FILTER_TYPE);
        map.put("search_text", str);
        new HttpRequester((DeliveryRouteActivity) activity, map, Const.ServiceCode.GET_CUSTOMER, false, this);

    }

    private void showAdvanceDialog() {
        final Dialog dialog = new Dialog(activity,
                R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(ContextCompat.getColor(activity, R.color.color_black_transparent)));
        dialog.setContentView(R.layout.dialog_adavance);

        TextView tvName = (TextView) dialog.findViewById(R.id.tvName);
        final EditText etAddress = (EditText) dialog.findViewById(R.id.etAddress);
        final EditText etCity = (EditText) dialog.findViewById(R.id.etCity);
        final EditText etZip = (EditText) dialog.findViewById(R.id.etZip);
        final TextView tvBack = (TextView) dialog.findViewById(R.id.tvBack);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });
        tvName.setText(pHelper.getName());
        TextView tvSearch = (TextView) dialog.findViewById(R.id.tvSearch);
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                ((DeliveryRouteActivity) activity).address = etAddress.getText().toString();
                ((DeliveryRouteActivity) activity).city = etCity.getText().toString();
                ((DeliveryRouteActivity) activity).zip = etZip.getText().toString();

                getCutomerAdvanse(((DeliveryRouteActivity) activity).address, ((DeliveryRouteActivity) activity).city, ((DeliveryRouteActivity) activity).zip);
            }
        });

        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void getCutomerAdvanse(String address, String city, String zip) {

        String search = "";
        search = ((DeliveryRouteActivity) activity).etSearch.getText().toString();

        if (!AndyUtils.isNetworkAvailable(activity)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    activity);
            return;
        }

        AndyUtils.showSimpleProgressDialog(activity);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.ADVANCE_SEARCH);

        map.put("id", pHelper.getUserId());
        map.put("zone_id", Const.ZONE_ID);
        map.put("type_fillter", Const.FILTER_TYPE);
        map.put("address", address);
        map.put("city", city);
        map.put("zip", zip);

        map.put("search_text", search);

        Log.d("ADress", address + " city " + city + " zip " + zip);

        new HttpRequester(activity, map, Const.ServiceCode.GET_CUSTOMER, false, this);
    }

    private void initMAP() {
        mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.fragmentMap);
        try {
            MapsInitializer.initialize(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the
        // map.
        if (map == null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
    }

    public void myClickMethod(View v) {


    }

    public Dialog showqbankselect(final List<Customer> listRouteTemp) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_qbank);
        dialog.setTitle(null);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(ContextCompat.getColor(getActivity(), android.R.color.transparent)));
        ListView listview = (ListView) dialog.findViewById(R.id.listview);
        QuetionBankAdapter adpter = new QuetionBankAdapter(getActivity(), listRouteTemp);
        listview.setAdapter(adpter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("", "SSSSSSS");
                Customer data = ((DeliveryRouteActivity) activity).listRouteTemp.get(position);
                tvNextLoction.setText(data.getAddress());
                secondLat = Double.parseDouble(data.getLatitude());
                secondLong = Double.parseDouble(data.getLongitude());
                double dist = AndyUtils.distance(firstLat, firstLong, secondLat, secondLong);
                tvDistane.setText(String.format("%.2f", dist) + " KM");

                Intent intent = new Intent(activity, DetailActivity.class);
                intent.putExtra("data", data);
                activity.startActivity(intent);
            }
        });

        TextView close = (TextView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.show();
        return dialog;
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        ParseContent parseContent = new ParseContent(activity);
        AndyUtils.removeSimpleProgressDialog();
        switch (serviceCode) {
            case Const.ServiceCode.GET_CUSTOMER:

                ((DeliveryRouteActivity) activity).listRoute.clear();
                if (parseContent.isStatusOk(response)) {
                    ((DeliveryRouteActivity) activity).listRoute = parseContent.getListCustomer(response);
                }
                map.clear();
                String data = "Zone-" + Const.ZONE_NAME + "," + Const.FILTER_TYPE.toUpperCase();
                ((DeliveryRouteActivity) activity).tvAddress.setText(data);
                tvDistane.setText("0.00 KM");
                tvNextLoction.setText("");
                if (((DeliveryRouteActivity) activity).listRoute.size() > 0) {

                    ((DeliveryRouteActivity) activity).tvAddress.setText(data + "," + ((DeliveryRouteActivity) activity).listRoute.get(0).getCompleted_customers()
                            + "/" + ((DeliveryRouteActivity) activity).listRoute.get(0).getTotal_customers());
                    drawPath();
                }
                break;
            case Const.ServiceCode.SEARCH:


                break;
            case Const.ServiceCode.GET_CUSTOMER_DUPLICATE:
                ((DeliveryRouteActivity) activity).listRouteTemp.clear();
                if (parseContent.isStatusOk(response)) {
                    ((DeliveryRouteActivity) activity).listRouteTemp = parseContent.getListCustomer(response);
                }
                showqbankselect(((DeliveryRouteActivity) activity).listRouteTemp);
                break;
            case Const.ServiceCode.DRAW_PATH_CLIENT:
                if (!TextUtils.isEmpty(response)) {

                    Log.d("ROUTE ::: ===> ", response);
                    routeClient = new BeanRoute();
                    routeClient = parseContent.parseRoute(response, routeClient);

                    final ArrayList<BeanStep> step = routeClient.getListStep();
                    pointsClient = new ArrayList<LatLng>();
                    lineOptionsClient = new PolylineOptions();

                    for (int i = 0; i < step.size(); i++) {
                        List<LatLng> path = step.get(i).getListPoints();
                        pointsClient.addAll(path);
                    }
                    if (polyLineClient != null)
                        polyLineClient.remove();
                    lineOptionsClient.addAll(pointsClient);
                    lineOptionsClient.width(4);
                    lineOptionsClient.color(activity.getResources().getColor(R.color.light_blue));

                    if (lineOptionsClient != null && map != null) {
                        polyLineClient = map.addPolyline(lineOptionsClient);
                    }
                }
        }

    }

    @Override
    public void onDestroyView() {
        SupportMapFragment f = (SupportMapFragment) getFragmentManager()
                .findFragmentById(R.id.fragmentMap);
        if (f != null) {
            try {
                getFragmentManager().beginTransaction().remove(f).commit();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        map = null;
        super.onDestroyView();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        map.setMyLocationEnabled(false);
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.getUiSettings().setZoomControlsEnabled(false);
        map.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {

            public void onCameraChange(CameraPosition camPos) {
                if (App.getAppInstance().currentZoom == -1) {
                    App.getAppInstance().currentZoom = camPos.zoom;
                } else if (camPos.zoom != App.getAppInstance().currentZoom) {
                    App.getAppInstance().currentZoom = camPos.zoom;
                    return;
                }
                App.getAppInstance().curretLatLng = camPos.target;
            }
        });
        if (map != null) {
            drawPath();
        }
    }

    public Bitmap resizeMapIcons(String iconName, int width, int height) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(iconName, "drawable", activity.getPackageName()));
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }

    private void drawPath() {


        LatLng first = null, second = null;
        PolylineOptions polylineOptions = new PolylineOptions();
        for (int i = 0; i < ((DeliveryRouteActivity) activity).listRoute.size(); i++) {

            double lat = Double.parseDouble(((DeliveryRouteActivity) activity).listRoute.get(i).getLatitude());
            double longi = Double.parseDouble(((DeliveryRouteActivity) activity).listRoute.get(i).getLongitude());
            LatLng latLongSourse = new LatLng(lat, longi);
            second = latLongSourse;

            if (i == 0) {

                tvNextLoction.setText(((DeliveryRouteActivity) activity).listRoute.get(i).getAddress());

                if (App.getAppInstance().curretLatLng == null) {
                    animateCameraToMarker(latLongSourse, true, 15);
                } else {
                    animateCameraToMarker(App.getAppInstance().curretLatLng, false, App.getAppInstance().currentZoom);
                }

                secondLat = lat;
                secondLong = longi;

                first = new LatLng(secondLat, secondLong);

            }

            if (((DeliveryRouteActivity) activity).listRoute.get(i).getChangeOverPinStatus().equals("green")) {

                Marker marker = map.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, longi))
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.mipmap.green_pin_map))


                );
                mapMarker
                        .put(marker, ((DeliveryRouteActivity) activity).listRoute.get(i));
            } else if (((DeliveryRouteActivity) activity).listRoute.get(i).getChangeOverPinStatus().equals("red")) {


                Marker marker = map.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, longi))
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.mipmap.red_img_pin))

                );

                // BitmapDescriptorFactory.fromBitmap(resizeMapIcons("image_name",100,100))
                //BitmapDescriptorFactory.fromResource(R.mipmap.red_pin)
                mapMarker
                        .put(marker, ((DeliveryRouteActivity) activity).listRoute.get(i));
            } else {
                Marker marker = map.addMarker(new MarkerOptions()
                        .position(new LatLng(lat, longi))
                        .icon(BitmapDescriptorFactory
                                .fromResource(R.mipmap.yellow_pin_map))

                );
                mapMarker
                        .put(marker, ((DeliveryRouteActivity) activity).listRoute.get(i));
            }
        }
        map.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {

                Customer data = mapMarker.get(marker);
                if (data.dupliate_flage != 0) {
                    if (!AndyUtils.isNetworkAvailable(activity)) {
                        AndyUtils.showToast(getResources().getString(R.string.no_internet),
                                activity);
                    }
                    AndyUtils.showSimpleProgressDialog(activity);
                    HashMap<String, String> map = new HashMap<>();
                    map.put(Const.URL, Const.ServiceType.GET_CUSTOMER);
                    map.put("id", pHelper.getUserId());
                    map.put("zone_id", Const.ZONE_ID);
                    map.put("customer_id", data.getID());
                    map.put("type_fillter", Const.FILTER_TYPE);
                    map.put("search_text", "");

                    new HttpRequester(activity, map, Const.ServiceCode.GET_CUSTOMER_DUPLICATE, false, MapViewFragment.this);
                } else {
                    tvNextLoction.setText(data.getAddress());
                    secondLat = Double.parseDouble(data.getLatitude());
                    secondLong = Double.parseDouble(data.getLongitude());
                    double dist = AndyUtils.distance(firstLat, firstLong, secondLat, secondLong);
                    tvDistane.setText(String.format("%.2f", dist) + " KM");

                    Intent intent = new Intent(activity, DetailActivity.class);
                    intent.putExtra("data", data);
                    activity.startActivity(intent);

                }

                return true;
            }

        });

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            checkLocationPermission();
        } else {

            locHelper = new LocationHelper(activity);
            locHelper.setLocationReceivedLister(this);
            locHelper.onStart();

            /* String url = Const.DIRECTION_API + "saddr=" + String.valueOf(myLocation.getLatitude()) + ","  + String.valueOf(myLocation.getLongitude()) + "&" + "daddr=" + event.getLatitude() + "," + event.getLongitude();
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);*/
        }
        double dist = AndyUtils.distance(firstLat, firstLong, secondLat, secondLong);
        tvDistane.setText(String.format("%.2f", dist) + " KM");
        drawPathToClient(first, second);
        //showPoly();
        // showPoly1();
    }

    private void showPoly() {

        /*LatLng latLongSourse = new LatLng(-33.7674232, 150.7961937);
        LatLng desti = new LatLng(-34.9075806,138.6067263);*/

       /* for (int i = 0; i < ((DeliveryRouteActivity) activity).listRoute.size(); i++) {

            double lat1= Double.parseDouble(((DeliveryRouteActivity) activity).listRoute.get(i).getLatitude());
            double longi= Double.parseDouble(((DeliveryRouteActivity) activity).listRoute.get(i).getLatitude());

            LatLng first = new LatLng(lat1,longi);

            if(i < (((DeliveryRouteActivity) activity).listRoute.size())-1) {

                double lat2= Double.parseDouble(((DeliveryRouteActivity) activity).listRoute.get(i+1).getLatitude());
                double long2= Double.parseDouble(((DeliveryRouteActivity) activity).listRoute.get(i+1).getLatitude());

                LatLng desti = new LatLng(lat2, long2);

                drawPathToClient(first,desti);
            }

        }
       */
    }

    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, Const.PERMISSION_LOCATION);
        } else {
            locHelper = new LocationHelper(activity);
            locHelper.setLocationReceivedLister(this);
            locHelper.onStart();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Const.PERMISSION_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        locHelper = new LocationHelper(activity);
                        locHelper.setLocationReceivedLister(this);
                        locHelper.onStart();
                    }
                }
                break;
        }
    }
   /* private  void showPoly1()
    {

        LatLng latLongSourse = new LatLng(-34.9075806, 138.6067263);
        LatLng desti = new LatLng(-34.9073267,138.6068538);

        drawPathToClient(latLongSourse,desti);

    }*/

    @Override
    public void onLocationReceived(LatLng latlong) {

    }

    @Override
    public void onLocationReceived(Location location) {

        if (location != null) {
            // drawTrip(latlong);
            myLocation = location;
        }
    }

    @Override
    public void onConnected(Bundle bundle) {


    }

    private void animateCameraToMarker(LatLng latLng, boolean isAnimate, float zoom) {
        try {
            CameraUpdate cameraUpdate = null;

            cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
            if (cameraUpdate != null && map != null) {
                if (isAnimate)
                    map.animateCamera(cameraUpdate);
                else
                    map.moveCamera(cameraUpdate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onConnected(Location location) {

        if (location != null) {

            myLocation = location;

            Log.d("===================> ", location.getLatitude() + " " + location.getLongitude());


            // isLocationEnable = true;
            LatLng latLang = new LatLng(location.getLatitude(),
                    location.getLongitude());
            App.getAppInstance().curretLatLng = latLang;


            firstLat = latLang.latitude;
            firstLong = latLang.longitude;


            getAddressFromLocation(latLang);

            AndyUtils.removeCustomProgressDialog();

            if (isLocationClick) {

                isLocationClick = false;


                String url = Const.DIRECTION_API + "saddr=" + String.valueOf(myLocation.getLatitude()) + "," + String.valueOf(myLocation.getLongitude()) + "&" + "daddr=" + secondLat + "," + secondLong;
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(browserIntent);

            }


        }


    }

    private void getAddressFromLocation(final LatLng latlng) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Geocoder gCoder = new Geocoder(getActivity());
                try {
                    final List<Address> list = gCoder.getFromLocation(
                            latlng.latitude, latlng.longitude, 1);
                    if (list != null && list.size() > 0) {
                        address = list.get(0);
                        String add = "";
                        StringBuilder sb = new StringBuilder();
                        if (address.getAddressLine(0) != null) {
                            if (address.getMaxAddressLineIndex() > 0) {
                                for (int i = 0; i < address
                                        .getMaxAddressLineIndex(); i++) {

                                    /*sb.append(address.getAddressLine(i))
                                            .append("\n");*/

                                    Log.d("address i ::: ", "" + address.getAddressLine(i));
                                    add = address.getAddressLine(i);

                                }
                                String cityStr[] = add.split(",");
                                String city = cityStr[0];

                                sb.append(city);


                                sb.append(",");
                                sb.append(address.getCountryName());

                                Log.d("address line ::: ", "" + address.getCountryName());

                            } else {
                                sb.append(address.getAddressLine(0));

                                Log.d("address else line ::: ", "" + address.getAddressLine(0));

                            }
                        }

                        strAddress = sb.toString();
                        strAddress = strAddress.replace(",null", "");
                        strAddress = strAddress.replace("null", "");
                        strAddress = strAddress.replace("Unnamed", "");

                        Log.d("ADDRESS ::::::::::::: ", strAddress);


                    }
                    if (getActivity() == null)
                        return;

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            tvMyLocation.setText(strAddress);

                        }
                    });
                } catch (Exception exc) {
                    exc.printStackTrace();
                }
            }
        }).start();
    }

    public void showLocationOffDialog() {
        AndyUtils.removeCustomProgressDialog();
        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(activity);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle(getString(R.string.dialog_no_location_service_title))
                .setMessage(getString(R.string.dialog_no_location_service))
                .setPositiveButton(getString(R.string.dialog_enable_location_service),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                removeLocationOffDialog();
                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                removeLocationOffDialog();
                                activity.finish();
                            }
                        });
        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.show();
    }

    public void removeLocationOffDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;
        }
    }

    @Override
    public void onStop() {
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(GpsChangeReceiver);
        super.onStop();
    }

    @Override
    public void onDestroy() {
        LocalBroadcastManager.getInstance(activity).unregisterReceiver(GpsChangeReceiver);
        super.onDestroy();
    }

    private void drawPathToClient(LatLng source, LatLng destination) {
        if (source == null || destination == null) {
            return;
        }
        HashMap<String, String> map = new HashMap<String, String>();
        map.put(Const.URL,
                "https://maps.googleapis.com/maps/api/directions/json?origin="
                        + source.latitude + "," + source.longitude
                        + "&destination=" + destination.latitude + ","
                        + destination.longitude + "&sensor=false&key="
                        + Const.DIRECTION_API_KEY);

        Log.d("Navigation Path",
                "https://maps.googleapis.com/maps/api/directions/json?origin="
                        + source.latitude + "," + source.longitude
                        + "&destination=" + destination.latitude + ","
                        + destination.longitude + "&sensor=false&key="
                        + Const.DIRECTION_API_KEY);
        new HttpRequester(activity, map,
                Const.ServiceCode.DRAW_PATH_CLIENT, true, this);
    }

    public class QuetionBankAdapter extends BaseAdapter {
        public LayoutInflater inflater;
        public Context con;
        public List<Customer> list;

        public QuetionBankAdapter(Context context, List<Customer> musiclist) {

            con = context;
            inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            list = musiclist;
        }

        public int getCount() {
            return list.size();
        }

        public Customer getItem(int position) {
            return list.get(position);
        }

        public long getItemId(int position) {
            return position;
        }

        public View getView(final int position, View convertview, ViewGroup parent) {

            ViewHolder holder;
            if (convertview == null) {
                holder = new ViewHolder();
                convertview = inflater.inflate(R.layout.row_list_view_duplicate, null);
                holder.tvPlaceName = (TextView) convertview.findViewById(R.id.tvPlaceName);
                holder.tvAddress = (TextView) convertview.findViewById(R.id.tvAddress);
                convertview.setTag(holder);
            } else {
                holder = (ViewHolder) convertview.getTag();
            }
            holder.tvPlaceName.setText(list.get(position).getCustomerName());
            holder.tvAddress.setText(list.get(position).getAddress());

            return convertview;
        }

        public class ViewHolder {
            TextView tvPlaceName, tvAddress;
        }

    }
}
