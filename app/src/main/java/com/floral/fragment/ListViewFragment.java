package com.floral.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.floral.AdvanceSearchActivity;
import com.floral.DeliveryRouteActivity;
import com.floral.R;
import com.floral.adapter.ListViewAdapter;
import com.floral.location.LocationHelper;
import com.floral.parse.HttpRequester;
import com.floral.parse.ParseContent;
import com.floral.utils.AndyUtils;
import com.floral.utils.Const;
import com.google.android.gms.maps.model.LatLng;

import java.util.HashMap;

public class ListViewFragment extends BaseFragment implements LocationHelper.OnLocationReceived {


    private View view;
    private RecyclerView recyclerView;
    private ListViewAdapter adapter;
    private TextView tvNoresult;
    private String data;
    private AlertDialog gpsAlertDialog;
    private boolean isGpsDialogShowing;
    public BroadcastReceiver GpsChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            final LocationManager manager = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);
            if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                removeLocationOffDialog();
            } else {
                if (isGpsDialogShowing) {
                    return;
                }
                showLocationOffDialog();
            }
        }
    };
    private LocationHelper locationHelper;
    private Location myLocation;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list_view, container, false);

        initUI();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onResume() {
        super.onResume();


        if (TextUtils.isEmpty(((DeliveryRouteActivity) activity).etSearch.getText().toString())) {
            if (TextUtils.isEmpty(((DeliveryRouteActivity) activity).address) && TextUtils.isEmpty(((DeliveryRouteActivity) activity).city) && TextUtils.isEmpty(((DeliveryRouteActivity) activity).zip)) {

                getCustomer();
            } else {
                getCutomerAdvanse(((DeliveryRouteActivity) activity).address, ((DeliveryRouteActivity) activity).city, ((DeliveryRouteActivity) activity).zip);

            }
        } else if (!TextUtils.isEmpty(((DeliveryRouteActivity) activity).etSearch.getText().toString())) {
            if (TextUtils.isEmpty(((DeliveryRouteActivity) activity).address) && TextUtils.isEmpty(((DeliveryRouteActivity) activity).city) && TextUtils.isEmpty(((DeliveryRouteActivity) activity).zip)) {

                searchCustomer(((DeliveryRouteActivity) activity).etSearch.getText().toString());
            } else {
                getCutomerAdvanse(((DeliveryRouteActivity) activity).address, ((DeliveryRouteActivity) activity).city, ((DeliveryRouteActivity) activity).zip);

            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case Const.PERMISSION_LOCATION:
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                        locationHelper = new LocationHelper(activity);
                        locationHelper.setLocationReceivedLister(this);
                        locationHelper.onStart();


                    }
                }
                break;


        }
    }

    public void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(activity, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, Const.PERMISSION_LOCATION);
        } else {

            locationHelper = new LocationHelper(activity);
            locationHelper.setLocationReceivedLister(this);
            locationHelper.onStart();
        }
    }

    private void initUI() {
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        //adapter = new ListViewAdapter(activity, ((DeliveryRouteActivity) activity).listRoute);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(activity);
        gridLayoutManager.supportsPredictiveItemAnimations();
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.getRecycledViewPool().clear();
        //recyclerView.setAdapter(adapter);

        tvNoresult = (TextView) view.findViewById(R.id.tvNoresult);


        ((DeliveryRouteActivity) activity).etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {


                    InputMethodManager inputMethodManager = (InputMethodManager) activity
                            .getSystemService(Activity.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus()
                            .getWindowToken(), 0);

                    if (!TextUtils.isEmpty(((DeliveryRouteActivity) activity).etSearch.getText().toString()))
                        searchCustomer(((DeliveryRouteActivity) activity).etSearch.getText().toString());
                    return true;
                }

                return false;
            }
        });


        ((DeliveryRouteActivity) activity).etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0) {
                    ((DeliveryRouteActivity) activity).tvClose.setVisibility(View.VISIBLE);
                }

            }
        });

        ((DeliveryRouteActivity) activity).tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DeliveryRouteActivity) activity).etSearch.setText("");

                getCustomer();

                ((DeliveryRouteActivity) activity).tvClose.setVisibility(View.GONE);

            }
        });

        ((DeliveryRouteActivity) activity).tvAdvance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // showAdvanceDialog();

                Intent intent = new Intent(activity, AdvanceSearchActivity.class);
                startActivity(intent);

            }
        });


        ((DeliveryRouteActivity) activity).btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ((DeliveryRouteActivity) activity).drawerLayout.closeDrawer(Gravity.RIGHT);

                getFilters();

            }
        });

        data = Const.ADDRESS;
    }

    private void getFilters() {
        if (!AndyUtils.isNetworkAvailable(activity)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    activity);
            return;
        }

        AndyUtils.showSimpleProgressDialog(activity);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.SEARCH);

        map.put("id", pHelper.getUserId());
        map.put("zone_id", Const.ZONE_ID);
        map.put("type_fillter", Const.FILTER_TYPE);

        new HttpRequester(activity, map, Const.ServiceCode.GET_CUSTOMER, false, this);
    }

    private void getCustomer() {


        if (!AndyUtils.isNetworkAvailable(activity)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    activity);
            return;
        }

        AndyUtils.showSimpleProgressDialog(activity);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.GET_CUSTOMER);

        map.put("id", pHelper.getUserId());
        map.put("zone_id", Const.ZONE_ID);
        map.put("type_fillter", Const.FILTER_TYPE);

        new HttpRequester(activity, map, Const.ServiceCode.GET_CUSTOMER, false, this);
    }

    public void myClickMethod(View v) {

        //getCustomer();
    }

    public void searchCustomer(String str) {


        AndyUtils.showSimpleProgressDialog(activity);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.SEARCH);

        map.put("id", pHelper.getUserId());
        map.put("zone_id", Const.ZONE_ID);
        map.put("type_fillter", Const.FILTER_TYPE);
        map.put("search_text", str);

        new HttpRequester((DeliveryRouteActivity) activity, map, Const.ServiceCode.GET_CUSTOMER, false, this);

    }

    public void showLocationOffDialog() {
        AndyUtils.removeCustomProgressDialog();
        isGpsDialogShowing = true;
        AlertDialog.Builder gpsBuilder = new AlertDialog.Builder(activity);
        gpsBuilder.setCancelable(false);
        gpsBuilder
                .setTitle(getString(R.string.dialog_no_location_service_title))
                .setMessage(getString(R.string.dialog_no_location_service))
                .setPositiveButton(getString(R.string.dialog_enable_location_service),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                Intent intent = new Intent(
                                        android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                startActivity(intent);
                                removeLocationOffDialog();
                            }
                        })

                .setNegativeButton(getString(R.string.dialog_exit),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                removeLocationOffDialog();
                                activity.finish();
                            }
                        });
        gpsAlertDialog = gpsBuilder.create();
        gpsAlertDialog.show();
    }

    public void removeLocationOffDialog() {
        if (gpsAlertDialog != null && gpsAlertDialog.isShowing()) {
            gpsAlertDialog.dismiss();
            isGpsDialogShowing = false;
            gpsAlertDialog = null;
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        ParseContent parseContent = new ParseContent(activity);
        AndyUtils.removeSimpleProgressDialog();

        switch (serviceCode) {
            case Const.ServiceCode.GET_CUSTOMER:


                //afret adavnce search clear it
                ((DeliveryRouteActivity) activity).address = "";
                ((DeliveryRouteActivity) activity).city = "";
                ((DeliveryRouteActivity) activity).zip = "";


                ((DeliveryRouteActivity) activity).listRoute.clear();
                if (parseContent.isStatusOk(response)) {
                    ((DeliveryRouteActivity) activity).listRoute = parseContent.getListCustomer(response);

                }

                data = "Zone-" + Const.ZONE_NAME + "," + Const.FILTER_TYPE.toUpperCase();

                Log.d("DETAIL DATA :::: ", data);

                ((DeliveryRouteActivity) activity).tvAddress.setText(data);


                if (((DeliveryRouteActivity) activity).listRoute.size() > 0) {

                    tvNoresult.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);

                    adapter = new ListViewAdapter(activity, ((DeliveryRouteActivity) activity).listRoute, locationHelper, ListViewFragment.this, GpsChangeReceiver);
                    recyclerView.getRecycledViewPool().clear();
                    recyclerView.setAdapter(adapter);

                    ((DeliveryRouteActivity) activity).tvAddress.setText(data + "," + ((DeliveryRouteActivity) activity).listRoute.get(0).getCompleted_customers()
                            + "/" + ((DeliveryRouteActivity) activity).listRoute.get(0).getTotal_customers());

                } else {
                    tvNoresult.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }


                break;
            case Const.ServiceCode.SEARCH:

                break;
        }

    }


    private void showAdvanceDialog() {
       /* final Dialog dialog = new Dialog(activity,
                R.style.MyDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(ContextCompat.getColor(activity, R.color.color_black_transparent)));
        dialog.setContentView(R.layout.dialog_adavance);

        TextView tvName = (TextView) dialog.findViewById(R.id.tvName);
       final EditText etAddress = (EditText) dialog.findViewById(R.id.etAddress);
        final EditText etCity = (EditText) dialog.findViewById(R.id.etCity);
        final EditText etZip = (EditText) dialog.findViewById(R.id.etZip);
        final TextView tvBack= (TextView) dialog.findViewById(R.id.tvBack);
        tvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });

        tvName.setText(pHelper.getName());

        TextView tvSearch = (TextView) dialog.findViewById(R.id.tvSearch);
        tvSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                dialog.dismiss();

                ((DeliveryRouteActivity)activity).address = etAddress.getText().toString();
                ((DeliveryRouteActivity)activity).city = etCity.getText().toString();
                ((DeliveryRouteActivity)activity). zip = etZip.getText().toString();


                getCutomerAdvanse(((DeliveryRouteActivity)activity).address, ((DeliveryRouteActivity)activity).city, ((DeliveryRouteActivity)activity).zip);
            }
        });

        TextView tvCancel = (TextView) dialog.findViewById(R.id.tvCancel);
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();*/

          /*  LayoutInflater layoutInflater
                    = (LayoutInflater)activity.getBaseContext()
                    .getSystemService(activity.LAYOUT_INFLATER_SERVICE);
            View popupView = layoutInflater.inflate(R.layout.dialog_adavance, null);
            final PopupWindow popupWindow = new PopupWindow(
                    popupView,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
          //  popupWindow.showAsDropDown(((DeliveryRouteActivity)activity).llDeliveryRoute, 0, 0, Gravity.TOP);

       // popupWindow.showAtLocation(((DeliveryRouteActivity)activity).llMainBasic, Gravity.TOP, 200, 125);
        int location[] = new int[2];

        // Get the View's(the one that was clicked in the Fragment) location
        ((DeliveryRouteActivity)activity).llMainBasic.getLocationOnScreen(location);

        // Using location, the PopupWindow will be displayed right under anchorView
        popupWindow.showAtLocation(((DeliveryRouteActivity)activity).llMainBasic, Gravity.NO_GRAVITY,
                location[0], location[1] - ((DeliveryRouteActivity)activity).llMainBasic.getHeight());*/


    }


    public void getCutomerAdvanse(String address, String city, String zip) {

        Log.d("FROM ADVANSE ::::::::: ", "==========>");

        String search = "";
        search = ((DeliveryRouteActivity) activity).etSearch.getText().toString();

        if (!AndyUtils.isNetworkAvailable(activity)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    activity);
            return;
        }

        AndyUtils.showSimpleProgressDialog(activity);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.ADVANCE_SEARCH);

        map.put("id", pHelper.getUserId());
        map.put("zone_id", Const.ZONE_ID);
        map.put("type_fillter", Const.FILTER_TYPE);
        map.put("address", address);
        map.put("city", city);
        map.put("zip", zip);

        map.put("search_text", search);

        Log.d("ADress", address + " city " + city + " zip " + zip);

        new HttpRequester(activity, map, Const.ServiceCode.GET_CUSTOMER, false, this);
    }


    @Override
    public void onLocationReceived(LatLng latlong) {

    }

    @Override
    public void onLocationReceived(Location location) {
        if (location != null)
            myLocation = location;
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnected(Location location) {
        AndyUtils.removeCustomProgressDialog();
        if (location != null) {
            myLocation = location;

            String url = Const.DIRECTION_API + "saddr=" + String.valueOf(myLocation.getLatitude())
                    + "," + String.valueOf(myLocation.getLongitude()) + "&"
                    + "daddr=" +
                    Const.DESTINATION_LAT + "," + Const.DESTINATION_LONG;
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        } else {
            showLocationOffDialog();
        }
    }

}
