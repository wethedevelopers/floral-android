package com.floral.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.floral.parse.AsyncTaskCompleteListener;
import com.floral.parse.ParseContent;
import com.floral.utils.PreferenceHelper;

public abstract class BaseFragment extends Fragment implements View.OnClickListener, AsyncTaskCompleteListener
{

    Activity activity;
    protected PreferenceHelper pHelper;
    protected ParseContent pContent;
    private Dialog otpDialog;
    private EditText etOtp;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = getActivity();
        pContent = new ParseContent(activity);
        pHelper = new PreferenceHelper(activity);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {




        }
    }



    public abstract void myClickMethod(View v);
    public abstract void searchCustomer(String searchString);

}
