package com.floral;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.google.android.gms.maps.model.LatLng;

public class App extends Application {

    public static App instance;
    public float currentZoom = -1;
    public LatLng curretLatLng;

    public App() {
        setInstance(this);
    }

    public static App getAppInstance() {
        return instance;
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setInstance(App instance) {
        App.instance = instance;
    }


}
