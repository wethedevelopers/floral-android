package com.floral;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.floral.parse.AsyncTaskCompleteListener;
import com.floral.parse.HttpRequester;
import com.floral.parse.ParseContent;
import com.floral.utils.AndyUtils;
import com.floral.utils.Const;
import com.floral.utils.PreferenceHelper;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements AsyncTaskCompleteListener{


    private TextView tvLogin;
    private EditText etEmail,etPwd;
    private PreferenceHelper preferenceHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferenceHelper = new PreferenceHelper(LoginActivity.this);

        if(preferenceHelper.getUserId() == null) {
            setContentView(R.layout.activity_login);

            initUI();
        }else
        {
            Intent intent = new Intent(LoginActivity.this, SelectZoneActivity.class);
            startActivity(intent);
            finish();

        }

    }

    private void initUI()
    {
        tvLogin=(TextView) findViewById(R.id.tvLogin);
        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isValidate())
                {
                    login();
                }

            }
        });

        etEmail =(EditText)findViewById(R.id.etEmail);
        etPwd = (EditText)findViewById(R.id.etPwd);

    }

    public boolean isValidate() {
        String msg = null;
        if (TextUtils.isEmpty(etEmail.getText().toString())
                && TextUtils.isEmpty(etEmail.getText().toString())) {
            msg = getResources().getString(R.string.text_enter_email);
        }
        else if (TextUtils.isEmpty(etPwd.getText().toString()))
        {
            msg = getResources().getString(R.string.text_enter_password);
        }
        else if (!TextUtils.isEmpty(etEmail.getText().toString())) {
            if (!AndyUtils.eMailValidation(etEmail.getText().toString())) {
                msg = getResources().getString(R.string.text_enter_valid_email);
            }
        }

        if (msg == null)
            return true;

        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
        return false;
    }

    private void login() {

        if (!AndyUtils.isNetworkAvailable(LoginActivity.this)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    LoginActivity.this);
            return;
        }

        if (isValidate()) {
            AndyUtils.showSimpleProgressDialog(LoginActivity.this);
            HashMap<String, String> map = new HashMap<>();
            map.put(Const.URL, Const.ServiceType.login);
            map.put(Const.Params.EMAIL, etEmail.getText().toString());
            map.put(Const.Params.PASSWORD, etPwd.getText().toString());

            new HttpRequester(LoginActivity.this, map, Const.ServiceCode.LOGIN, false, this);
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        ParseContent parseContent = new ParseContent(LoginActivity.this);
        AndyUtils.removeSimpleProgressDialog();

        switch (serviceCode) {
            case Const.ServiceCode.LOGIN:
                if (parseContent.isStatusOk(response)) {

                    parseContent.isSuccessWithStoreId(response);

                    Intent intent = new Intent(LoginActivity.this, SelectZoneActivity.class);
                    startActivity(intent);
                    finish();


                }
        }
    }


}
