package com.floral;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.floral.adapter.HistoryGridAdapter;
import com.floral.component.MarginDecoration;
import com.floral.model.Customer;
import com.floral.model.CustomerNew;
import com.floral.parse.HttpRequester;
import com.floral.parse.ParseContent;
import com.floral.utils.AndyUtils;
import com.floral.utils.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class HistoryActivity extends BaseActivity {

    public TextView tvName;
    private TextView tvBack;
    private HistoryGridAdapter gridAdpt;
    private RecyclerView recyclerView;
    private Customer data;
    private ArrayList<CustomerNew> dataParse;
    private String customer_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_new);
        customer_name = getIntent().getStringExtra("customer_name");
        initUI();
    }


    private void initUI() {

        data = new Customer();

        Bundle b = getIntent().getExtras();

        if (b != null) {
            data = (Customer) b.getSerializable("data");
        }

        tvBack = (TextView) findViewById(R.id.tvBack);
        tvBack.setOnClickListener(this);
        tvName = (TextView) findViewById(R.id.tvName);
        tvName.setText(customer_name);

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.addItemDecoration(new MarginDecoration(3, 10, true, 0));
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(HistoryActivity.this, 3);
        gridLayoutManager.supportsPredictiveItemAnimations();
        recyclerView.setLayoutManager(gridLayoutManager);
        getHistory();

    }

    private void getHistory() {
        if (!AndyUtils.isNetworkAvailable(HistoryActivity.this)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    HistoryActivity.this);
            return;
        }
        AndyUtils.showSimpleProgressDialog(HistoryActivity.this);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.HISTORY);
        map.put("customer_id", data.getID());
        new HttpRequester(HistoryActivity.this, map, Const.ServiceCode.HISTORY, false, this);
    }


    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        super.onTaskCompleted(response, serviceCode);
        ParseContent parseContent = new ParseContent(HistoryActivity.this);
        AndyUtils.removeSimpleProgressDialog();

        switch (serviceCode) {
            case Const.ServiceCode.HISTORY:
                if (parseContent.isStatusOk(response)) {
                    //dataParse = parseContent.getChangeOverHistory(response);
                    dataParse = parseContent.getChangeOverHistoryNew(response);
                    tvName.setText(dataParse.get(0).customer_name);
                    gridAdpt = new HistoryGridAdapter(HistoryActivity.this, dataParse);
                    recyclerView.getRecycledViewPool().clear();
                    recyclerView.setAdapter(gridAdpt);
                }

                break;

        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()) {
            case R.id.tvBack:

                finish();

                break;

            case R.id.tvPrevious:
                getHistory();

                break;

            case R.id.tvNext:


                getHistory();

                break;
        }
    }
}
