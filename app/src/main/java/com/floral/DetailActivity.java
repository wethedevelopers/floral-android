package com.floral;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.floral.model.Customer;

public class DetailActivity extends BaseActivity implements View.OnClickListener {

    private TextView tvBack, tvNameUSer, tvName, tvAddress, tvPhone, tvEmail, tvNote, tvProduct, tvTotlaUnit, tvStage, tvChangeDt, tvUpadteStatus;

    private Customer data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflater.inflate(R.layout.activity_detail, flContainer);
        initUI();
    }

    private void initUI() {

        data = new Customer();

        tvBack = (TextView) findViewById(R.id.tvBack);
        tvBack.setOnClickListener(this);
        tvNameUSer = (TextView) findViewById(R.id.tvNameUSer);

        tvName = (TextView) findViewById(R.id.tvName);
        tvAddress = (TextView) findViewById(R.id.tvAddress);
        tvPhone = (TextView) findViewById(R.id.tvPhone);
        tvEmail = (TextView) findViewById(R.id.tvEmail);
        tvNote = (TextView) findViewById(R.id.tvNote);
        tvProduct = (TextView) findViewById(R.id.tvProduct);
        tvTotlaUnit = (TextView) findViewById(R.id.tvTotlaUnit);
        tvStage = (TextView) findViewById(R.id.tvStage);
        tvChangeDt = (TextView) findViewById(R.id.tvChangeDt);
        tvUpadteStatus = (TextView) findViewById(R.id.tvUpadteStatus);
        tvUpadteStatus.setOnClickListener(this);


        Bundle b = getIntent().getExtras();
        if (b != null) {
            data = (Customer) b.getSerializable("data");
        }

        tvNameUSer.setText(preferenceHelper.getName());

        tvName.setText(data.getCustomerName());
        tvAddress.setText(data.getAddress());
        tvPhone.setText(data.getContactNumber());
        tvEmail.setText(data.getEmail());
        tvNote.setText(data.getNotes());
        tvProduct.setText(data.getProducts());
        tvTotlaUnit.setText(data.getTotalUnits());
        tvStage.setText(data.getStage());
        tvChangeDt.setText(data.getChangeOverDate());
        tvPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + data.getContactNumber()));
                startActivity(intent);
            }
        });
        tvEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("plain/text");
                i.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{data.getEmail()});
                i.putExtra(android.content.Intent.EXTRA_SUBJECT, data.getCustomerName());
                i.putExtra(android.content.Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(i, "Send email"));
            }
        });
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);

        switch (view.getId()) {
            case R.id.tvUpadteStatus:
                Intent intent = new Intent(DetailActivity.this, UpdateStatusActivity.class);
                intent.putExtra("data", data);
                startActivity(intent);
                finish();
                break;

            case R.id.tvBack:

                finish();

                break;

        }
    }
}
