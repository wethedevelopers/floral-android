package com.floral.parse;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.floral.utils.AndyUtils;
import com.floral.utils.Const;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Map;
import java.util.concurrent.Executors;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class HttpRequester {
    private HttpURLConnection httpURLConnection;
    Context context;
    private Map<String, String> map;
    private AsyncTaskCompleteListener asyncTaskCompleteListener;
    private int serviceCode;
    private String response = "";
    HttpUrlConnectionAsyncTask httpUrlConnectionAsyncTask;
    private boolean isGet;

    DataOutputStream dataOutputStream = null;
    String lineEnd = "\r\n", twoHyphens = "--", boundary = "*****" + Long.toString(System.currentTimeMillis()) + "*****";
    int bytesRead, bytesAvailable, bufferSize;
    byte[] buffer;
    File file;
    int maxBufferSize = 1024 * 1024;
    FileInputStream fileInputStream;

    public HttpRequester(Context context, Map<String, String> map, int serviceCode, AsyncTaskCompleteListener asyncTaskCompleteListener) {
        this.context = context;
        this.map = map;
        this.serviceCode = serviceCode;
        if (AndyUtils.isNetworkAvailable((Activity) context)) {
            this.asyncTaskCompleteListener = asyncTaskCompleteListener;
            httpUrlConnectionAsyncTask = (HttpUrlConnectionAsyncTask) new HttpUrlConnectionAsyncTask().executeOnExecutor(Executors.newSingleThreadExecutor(), map.get(Const.URL));
        }
    }

    public HttpRequester(Context context, Map<String, String> map, int serviceCode, boolean isGet, AsyncTaskCompleteListener asyncTaskCompleteListener) {
        this.context = context;
        this.map = map;
        this.serviceCode = serviceCode;
        this.isGet = isGet;
        if (AndyUtils.isNetworkAvailable((Activity) context)) {
            this.asyncTaskCompleteListener = asyncTaskCompleteListener;
            httpUrlConnectionAsyncTask = (HttpUrlConnectionAsyncTask) new HttpUrlConnectionAsyncTask().executeOnExecutor(Executors.newSingleThreadExecutor(), map.get(Const.URL));
        }
    }

    private class HttpUrlConnectionAsyncTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(map.get(Const.URL));


                Log.i("----->>> url", map.get(Const.URL));

                disableSSLCertificateChecking();


                httpURLConnection = (HttpURLConnection) url.openConnection();
                if(!isGet) {
                    map.remove(Const.URL);
                    httpURLConnection.setRequestMethod("POST");
                    httpURLConnection.setDoOutput(true);
                    httpURLConnection.setDoInput(true);
                    httpURLConnection.setUseCaches(false); // Don't use a Cached Copy
                    httpURLConnection.setRequestProperty("Connection", "Keep-Alive");
                    httpURLConnection.setRequestProperty("ENCTYPE", "multipart/form-data"); // new
                    httpURLConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                    dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());

                    for (String key : map.keySet()) {
                        if (key.equalsIgnoreCase("changeOver_Image") && !TextUtils.isEmpty(map.get(key))) {
                            Log.i(key , map.get(key));

                            file = new File(map.get(key));
                            fileInputStream = new FileInputStream(file);

                            dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                            dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"; filename=\"" + file.getName() + "\"" + lineEnd);
                            dataOutputStream.writeBytes("Content-Type: image/jpeg" + lineEnd);
                            dataOutputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);
                            dataOutputStream.writeBytes(lineEnd);

                            bytesAvailable = fileInputStream.available();
                            bufferSize = Math.min(bytesAvailable, maxBufferSize);
                            buffer = new byte[bufferSize];

                            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                            while (bytesRead > 0) {
                                dataOutputStream.write(buffer, 0, bufferSize);
                                bytesAvailable = fileInputStream.available();
                                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                            }

                            dataOutputStream.writeBytes(lineEnd);

                        } else {
                            dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
                            dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                            dataOutputStream.writeBytes("Content-Type: text/plain" + lineEnd);
                            if(map.get(key) != null) {
                                Log.i(key, map.get(key));
                                dataOutputStream.writeBytes(lineEnd);
                                // assign value
                                dataOutputStream.writeBytes(map.get(key));
                                dataOutputStream.writeBytes(lineEnd);
                            }
                        }
                    }
                    // send multipart form data necesssary after file data...
                    dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    dataOutputStream.flush();
                    dataOutputStream.close();
                }

                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                    String inputLine;
                    StringBuilder builder = new StringBuilder();
                    while ((inputLine = bufferedReader.readLine()) != null) {
                        builder.append(inputLine);
                    }
                    response = builder.toString();
                    bufferedReader.close();
                } else
                    return response;

            } catch (Exception e) {
                e.printStackTrace();
            }
            httpURLConnection.disconnect();
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if (asyncTaskCompleteListener != null) {
                asyncTaskCompleteListener.onTaskCompleted(response, serviceCode);
            }
        }
    }

    private static void disableSSLCertificateChecking() {
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            @Override
            public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                // Not implemented
            }

            @Override
            public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
                // Not implemented
            }
        } };

        try {
            SSLContext sc = SSLContext.getInstance("TLS");

            sc.init(null, trustAllCerts, new java.security.SecureRandom());

            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }
}
