package com.floral.parse;

public interface AsyncTaskCompleteListener {
    void onTaskCompleted(String str, int i);
}
