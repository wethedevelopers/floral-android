package com.floral.parse;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.floral.MainActivity;
import com.floral.maputils.PolyLineUtils;
import com.floral.model.BeanRoute;
import com.floral.model.BeanStep;
import com.floral.model.Customer;
import com.floral.model.CustomerNew;
import com.floral.model.Inbox;
import com.floral.model.Zone;
import com.floral.utils.Const;
import com.floral.utils.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ParseContent {
    private final String KEY_ERROR = "error";
    private final String KEY_SUCCESS = "success";
    private final String KEY_STATUS = "status";
    private final String EVENT_ID = "event_id";
    private final String INVITATION_ID = "invitation_id";
    private final String SUBJECT = "subject";
    private final String NOTE = "note";
    private final String START_DATE = "start_date";
    private final String END_DATE = "end_date";
    private final String LATITUDE = "latitude";
    private final String LONGITUDE = "longitude";
    private final String ADDRESS = "address";
    private final String EVENT_PICTURE = "event_picture";
    private final String NAME = "name";
    private final String EVENTS = "events";
    private final String EVENT_DATA = "event_data";
    private final String INVITED_BY = "invited_by";
    private final String EVENT_USERS = "event_users";
    private final String USER_NAME = "user_name";
    private final String USER_PICTURE = "users_picture";
    private final String USER_ID = "user_id";
    private final String INVITEE_NUMBER = "invited_by_phone";
    private final String DIALOG_NAME = "dialogue_name";
    private final String OCCUPANT_ID = "occupent_id";
    private final String HOME_ADDRESS = "home_address";
    private Activity activity;
    private PreferenceHelper preferenceHelper;

    //private DBHelper dbhelper;


    public ParseContent(Activity activity) {
        this.activity = activity;
        preferenceHelper = new PreferenceHelper(activity);
    }

    public boolean isStatusOk(String response) {
        if (TextUtils.isEmpty(response)) {
            return false;
        }
        try {
            Log.d(Const.TAG, response);
            JSONObject jsonObject = new JSONObject(response);
            if (!jsonObject.getString(KEY_STATUS).equalsIgnoreCase("ok")) {
                Toast.makeText(activity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                return false;
            } else {
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean isStatusOkWithToast(String response) {
        if (TextUtils.isEmpty(response)) {
            return false;
        }
        try {
            Log.d(Const.TAG, response);
            JSONObject jsonObject = new JSONObject(response);
            if (!jsonObject.getString(KEY_STATUS).equalsIgnoreCase("ok")) {
                Toast.makeText(activity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                return false;
            } else {
                Toast.makeText(activity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                return true;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public String getUploadImage(String response) {
        String imgPath = null;
        try {
            Log.d(Const.TAG, response);
            JSONObject jsonObject = new JSONObject(response);
            if (!jsonObject.getString(KEY_STATUS).equalsIgnoreCase("ok")) {
                Toast.makeText(activity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
            } else {
                JSONObject jOnj = jsonObject.getJSONObject("data");
                imgPath = jOnj.getString("uploaded_image");
                Log.d("UPLOAD IMG :: ", imgPath);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return imgPath;
    }

    public Customer getChangeOverHistory(String response) {
        Customer data = new Customer();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString(KEY_STATUS).equalsIgnoreCase("ok")) {
                data.setPreviousYear(jsonObject.getInt("previous_year"));
                data.setPreviousMonth(jsonObject.getInt("previous_month"));
                data.setNext_month(jsonObject.getInt("next_month"));
                data.setNext_year(jsonObject.getInt("next_year"));
                data.setCustomerName(jsonObject.getString("customer_name"));
                data.setNotes(jsonObject.getString("customer_note"));
                data.setChangeOverDate(jsonObject.getString("ChangeOverDate"));
                JSONArray jImg = jsonObject.getJSONArray("image");

                ArrayList<String> listImg = new ArrayList<>();
                for (int j = 0; j < jImg.length(); j++) {
                    data.setChangeover_image(jImg.getString(j));

                    listImg.add(jImg.getString(j));
                }
                data.changeOverImg = listImg.toArray(data.changeOverImg);
            } else {
            }
        } catch (Exception e) {

            e.printStackTrace();
        }


        return data;

    }

    public ArrayList<CustomerNew> getChangeOverHistoryNew(String response) {

        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString(KEY_STATUS).equalsIgnoreCase("ok")) {
                JSONArray jdate = jsonObject.getJSONArray("Data");
                ArrayList<CustomerNew> arlist = new ArrayList<>();
                for (int i = 0; i < jdate.length(); i++) {
                    CustomerNew data = new CustomerNew();
                    JSONObject job = jdate.getJSONObject(i);
                    data.customer_name = job.getString("customer_name");
                    data.customer_note = job.getString("customer_note");
                    data.ChangeOverDate = job.getString("ChangeOverDate");
                    data.remarks = job.getString("remarks");
                    data.Remarks_yes = job.getString("Remarks_yes");
                    data.image = job.getString("image");
                    arlist.add(data);
                }
                return arlist;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public List<Customer> getListCustomer(String response) {
        ArrayList<Customer> listCustomer = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString(KEY_STATUS).equalsIgnoreCase("ok")) {


                int tot = jsonObject.getInt("total_customers");
                int completeCust = jsonObject.getInt("completed_customers");

                JSONArray jobjMain = jsonObject.getJSONArray("data");

                for (int i = 0; i < jobjMain.length(); i++) {
                    Customer data = new Customer();
                    JSONObject jsonObjData = jobjMain.getJSONObject(i);
                    data.setTotal_customers(tot);
                    data.setCompleted_customers(completeCust);
                    data.setID(jsonObjData.getString("ID"));
                    data.setCustomerName(jsonObjData.getString("CustomerName"));
                    data.setTotalUnits(jsonObjData.getString("TotalUnits"));
                    data.setChangeOverPinStatus(jsonObjData.getString("ChangeOverPinStatus"));
                    data.setAddress(jsonObjData.getString("Address"));
                    data.setLatitude(jsonObjData.getString("Latitude"));
                    data.setLongitude(jsonObjData.getString("Longitude"));
                    data.setContactNumber(jsonObjData.getString("ContactNumber"));
                    data.setNotes(jsonObjData.getString("Notes"));
                    data.setEmail(jsonObjData.getString("Email"));
                    data.setStage(jsonObjData.getString("Stage"));
                    data.setProducts(jsonObjData.getString("Products"));
                    data.setZoneID(jsonObjData.getString("ZoneID"));
                    data.setChangeOverDate(jsonObjData.getString("ChangeOverDate"));
                    data.dupliate_flage = jsonObjData.getInt("dupliate_flage");
                    JSONArray jImg = jsonObjData.getJSONArray("changeover_image");

                    ArrayList<String> listImg = new ArrayList<>();
                    for (int j = 0; j < jImg.length(); j++) {
                        data.setChangeover_image(jImg.getString(j));
                        listImg.add(jImg.getString(j));
                    }
                    data.changeOverImg = listImg.toArray(data.changeOverImg);
                    listCustomer.add(data);
                }


            } else {

            }
        } catch (Exception e) {

            e.printStackTrace();
        }


        return listCustomer;

    }

    public void isStatusOkOnlyDisplayMsg(String response) {

        try {
            Log.d(Const.TAG, response);
            JSONObject jsonObject = new JSONObject(response);
            if (!jsonObject.getString(KEY_STATUS).equalsIgnoreCase("ok")) {
                Toast.makeText(activity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(activity, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Zone> getZoneList(String response) {

        ArrayList<Zone> list = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString(KEY_STATUS).equalsIgnoreCase("ok")) {

                JSONArray jobjMain = jsonObject.getJSONArray("data");

                for (int i = 0; i < jobjMain.length(); i++) {
                    Zone data = new Zone();

                    JSONObject jsonObjData = jobjMain.getJSONObject(i);

                    data.setID(jsonObjData.getString("ID"));
                    data.setZoneName(jsonObjData.getString("ZoneName"));

                    list.add(data);
                }

            }
        } catch (Exception e) {

        }

        return list;

    }

    public BeanRoute parseRoute(String response, BeanRoute routeBean) {

        try {
            BeanStep stepBean;
            JSONObject jObject = new JSONObject(response);
            JSONArray jArray = jObject.getJSONArray("routes");
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject innerjObject = jArray.getJSONObject(i);
                if (innerjObject != null) {
                    JSONArray innerJarry = innerjObject.getJSONArray("legs");
                    for (int j = 0; j < innerJarry.length(); j++) {

                        JSONObject jObjectLegs = innerJarry.getJSONObject(j);
                        routeBean.setDistanceText(jObjectLegs.getJSONObject(
                                "distance").getString("text"));
                        routeBean.setDistanceValue(jObjectLegs.getJSONObject(
                                "distance").getInt("value"));

                        routeBean.setDurationText(jObjectLegs.getJSONObject(
                                "duration").getString("text"));
                        routeBean.setDurationValue(jObjectLegs.getJSONObject(
                                "duration").getInt("value"));

                        routeBean.setStartAddress(jObjectLegs
                                .getString("start_address"));
                        routeBean.setEndAddress(jObjectLegs
                                .getString("end_address"));

                        routeBean.setStartLat(jObjectLegs.getJSONObject(
                                "start_location").getDouble("lat"));
                        routeBean.setStartLon(jObjectLegs.getJSONObject(
                                "start_location").getDouble("lng"));
                        routeBean.setEndLat(jObjectLegs.getJSONObject(
                                "end_location").getDouble("lat"));
                        routeBean.setEndLon(jObjectLegs.getJSONObject(
                                "end_location").getDouble("lng"));

                        JSONArray jstepArray = jObjectLegs
                                .getJSONArray("steps");
                        if (jstepArray != null) {
                            for (int k = 0; k < jstepArray.length(); k++) {
                                stepBean = new BeanStep();
                                JSONObject jStepObject = jstepArray
                                        .getJSONObject(k);
                                if (jStepObject != null) {

                                    stepBean.setHtml_instructions(jStepObject
                                            .getString("html_instructions"));
                                    stepBean.setStrPoint(jStepObject
                                            .getJSONObject("polyline")
                                            .getString("points"));
                                    stepBean.setStartLat(jStepObject
                                            .getJSONObject("start_location")
                                            .getDouble("lat"));
                                    stepBean.setStartLon(jStepObject
                                            .getJSONObject("start_location")
                                            .getDouble("lng"));
                                    stepBean.setEndLat(jStepObject
                                            .getJSONObject("end_location")
                                            .getDouble("lat"));
                                    stepBean.setEndLong(jStepObject
                                            .getJSONObject("end_location")
                                            .getDouble("lng"));

                                    stepBean.setListPoints(new PolyLineUtils()
                                            .decodePoly(stepBean.getStrPoint()));
                                    routeBean.getListStep().add(stepBean);
                                }
                            }
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return routeBean;
    }


    public ArrayList<Inbox> getInbox(String response) {
        ArrayList<Inbox> list = new ArrayList<>();

        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString(KEY_STATUS).equalsIgnoreCase("ok")) {

                JSONArray jobjMain = jsonObject.getJSONArray("data");

                for (int i = 0; i < jobjMain.length(); i++) {
                    Inbox data = new Inbox();

                    JSONObject jobj = jobjMain.getJSONObject(i);
                    data.setID(jobj.getString("ID"));
                    data.setMessage(jobj.getString("Message"));
                    data.setIsRead(jobj.getString("IsRead"));
                    data.setSender(jobj.getString("Sender"));
                    data.setSentOn(jobj.getString("SentOn"));

                    list.add(data);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }


    public void isSuccessWithStoreId(String response) {

        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString(KEY_STATUS).equalsIgnoreCase("ok")) {

                JSONObject jobjMain = jsonObject.getJSONObject("data");

                preferenceHelper.putUserId(jobjMain
                        .getString(Const.Params.ID));
                preferenceHelper.putEmail(jobjMain
                        .optString(Const.Params.EMAIL));
                preferenceHelper.putName(jobjMain.optString(Const.Params.NAME));
                preferenceHelper.putPhoneNum(jobjMain.optString(Const.Params.PHONE));


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public boolean isSuccess(String response) {
        if (TextUtils.isEmpty(response)) {
            return false;
        }
        try {
            Log.d(Const.TAG, response);
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getBoolean(KEY_SUCCESS)) {
                return true;
            } else if (jsonObject.getString(KEY_ERROR).equalsIgnoreCase("2") || jsonObject.getString(KEY_ERROR).equalsIgnoreCase("1")) {
                new PreferenceHelper(activity).logout();
//                AndyUtils.showToast( activity.getString(R.string.error_12), activity);
                Intent intent = new Intent(activity, MainActivity.class);
                activity.startActivity(intent);
                activity.finish();
                return false;
            } else {
                showErrorToast(jsonObject.getInt("error"), activity);
                return false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void showErrorToast(int id, Context ctx) {
        Toast.makeText(ctx, this.activity.getResources().getString(this.activity.getResources().getIdentifier(Const.ERROR_CODE_PREFIX + id, "string", ctx.getPackageName())), Toast.LENGTH_LONG).show();
    }


}
