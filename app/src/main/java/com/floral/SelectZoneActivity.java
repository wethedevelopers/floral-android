package com.floral;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.floral.adapter.InboxAdapter;
import com.floral.adapter.ZoneAdpter;
import com.floral.model.Inbox;
import com.floral.parse.AsyncTaskCompleteListener;
import com.floral.parse.HttpRequester;
import com.floral.parse.ParseContent;
import com.floral.utils.AndyUtils;
import com.floral.utils.Const;
import com.floral.utils.PreferenceHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class SelectZoneActivity extends AppCompatActivity implements View.OnClickListener, AsyncTaskCompleteListener{

    private TextView tvSelectZone,tvStart,tvName;
    private PreferenceHelper preferenceHelper;
    //private ArrayList<Zone> list;
    private String zoneId,zoneName;

    //
    private InboxAdapter adapterInbox;
    private ArrayList<Inbox> listInbox;
    private RecyclerView recyclerViewInbox;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_zone);

        preferenceHelper = new PreferenceHelper(SelectZoneActivity.this);

        initUI();
        getZone();

    }

    public boolean isValidate()
    {
        String msg = null;
        if (tvSelectZone.getText().equals(getString(R.string.select_zone)))
        {
            msg = getResources().getString(R.string.text_select_zone);
        }

        if (msg == null)
            return true;

        Toast.makeText(SelectZoneActivity.this, msg, Toast.LENGTH_SHORT).show();
        return false;
    }

    private void getZone()
    {

        if (!AndyUtils.isNetworkAvailable(SelectZoneActivity.this)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    SelectZoneActivity.this);
            return;
        }


        AndyUtils.showSimpleProgressDialog(SelectZoneActivity.this);
            HashMap<String, String> map = new HashMap<>();
            map.put(Const.URL, Const.ServiceType.ZONE_LIST);
            map.put("id", preferenceHelper.getUserId());


            new HttpRequester(SelectZoneActivity.this, map, Const.ServiceCode.ZONE_LIST, false, this);
    }

    private void initUI()
    {

       // list = new ArrayList<>();
        tvSelectZone=(TextView) findViewById(R.id.tvSelectZone);
        tvSelectZone.setOnClickListener(this);
        tvStart= (TextView) findViewById(R.id.tvStart);
        tvStart.setOnClickListener(this);
        tvName = (TextView)findViewById(R.id.tvName);
        tvName.setText(preferenceHelper.getName());

        //Inbox

        final GestureDetector mGestureDetector = new GestureDetector(SelectZoneActivity.this, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });
        listInbox = new ArrayList<>();
        recyclerViewInbox = (RecyclerView) findViewById(R.id.recyclerView);
        //adapter = new ListViewAdapter(activity, ((DeliveryRouteActivity) activity).listRoute);
        recyclerViewInbox.setHasFixedSize(true);
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(SelectZoneActivity.this);
        gridLayoutManager.supportsPredictiveItemAnimations();
        recyclerViewInbox.setLayoutManager(gridLayoutManager);
        recyclerViewInbox.getRecycledViewPool().clear();

        recyclerViewInbox.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {


            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());


                if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                    // Toast.makeText(activity,"The Item Clicked is: "+recyclerView.getChildPosition(child),Toast.LENGTH_SHORT).show();

                    int pos = recyclerView.getChildPosition(child);


                    Intent intent = new Intent(SelectZoneActivity.this, InboxDetailActivity.class);
                    intent.putExtra("desc", listInbox.get(pos).getMessage());
                    startActivity(intent);

                    return true;

                }


                return false;
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }
        });

    }


    @Override
    public void onClick(View view) {

        switch (view.getId())
        {
            case R.id.tvSelectZone :

                showDialogSelectZone();

                break;

           case R.id.tvStart :


               if(isValidate()) {
                   Intent intent = new Intent(SelectZoneActivity.this, DeliveryRouteActivity.class);
                   intent.putExtra("zoneId",zoneId);
                   intent.putExtra("zoneName", zoneName);

                  // Const.ZONE_LIST.addAll(list);

                   //intent.putExtra("zoneList", list);

                   startActivity(intent);
                   finish();
               }

                break;
        }
    }

    private void showDialogSelectZone()
    {

       final Dialog zoneDialog = new Dialog(this,
                    R.style.MyDialog);
        zoneDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        zoneDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(ContextCompat.getColor(this, R.color.color_black_transparent)));
        zoneDialog.setContentView(R.layout.dialog_zone);

        final ListView listZone =(ListView) zoneDialog.findViewById(R.id.listZone);
        TextView tvCancel = (TextView) zoneDialog.findViewById(R.id.tvCancel);
        ZoneAdpter adapter= new ZoneAdpter(SelectZoneActivity.this,Const.ZONE_LIST);
        listZone.setAdapter(adapter);

        listZone.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                zoneId=Const.ZONE_LIST.get(i).getID();
                zoneName = Const.ZONE_LIST.get(i).getZoneName();
                Const.ZONE_LIST.get(i).setSelected(true);

                Log.d("selected zone :: ",""+zoneId);

                tvSelectZone.setText(Const.ZONE_LIST.get(i).getZoneName());


                zoneDialog.dismiss();

            }
        });




        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                zoneDialog.dismiss();
            }
        });


          zoneDialog.show();

    }
    private void getInbox()
    {

        if (!AndyUtils.isNetworkAvailable(SelectZoneActivity.this)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    SelectZoneActivity.this);
            return;
        }

        AndyUtils.showSimpleProgressDialog(SelectZoneActivity.this);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.INBOX);

        map.put("id", preferenceHelper.getUserId());

        new HttpRequester(SelectZoneActivity.this, map, Const.ServiceCode.INBOX, false, this);
    }


    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        ParseContent parseContent = new ParseContent(SelectZoneActivity.this);
        AndyUtils.removeSimpleProgressDialog();

        switch (serviceCode) {
            case Const.ServiceCode.ZONE_LIST:

                AndyUtils.removeCustomProgressDialog();
                getInbox();

                if (parseContent.isStatusOk(response))
                {
                    Const.ZONE_LIST= parseContent.getZoneList(response);
                }
                break;

            case Const.ServiceCode.INBOX:
               // if (parseContent.isStatusOk(response)) {

                    listInbox =parseContent.getInbox(response);

                    adapterInbox = new InboxAdapter(SelectZoneActivity.this, listInbox);
                    recyclerViewInbox.getRecycledViewPool().clear();
                    recyclerViewInbox.setAdapter(adapterInbox);

               // }


                break;
        }
    }
}
