package com.floral.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidquery.AQuery;
import com.androidquery.callback.ImageOptions;
import com.floral.HistoryActivity;
import com.floral.R;
import com.floral.UpdateStatusActivity;
import com.floral.model.CustomerNew;
import com.floral.utils.Const;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class HistoryGridAdapter extends RecyclerView.Adapter<HistoryGridAdapter.TextViewHolder> {
    private ArrayList<CustomerNew> arlist;
    private Context context;
    private AQuery aQuery;
    private ImageOptions imageOptions;
    private TextViewHolder holder;

    public HistoryGridAdapter(Context context, ArrayList<CustomerNew> arlist) {
        this.context = context;

        Log.d("COntext :: ", this.context + "");
        this.arlist = arlist;

        aQuery = new AQuery(context);
        imageOptions = new ImageOptions();
        imageOptions.memCache = true;
        imageOptions.fileCache = true;
        imageOptions.targetWidth = 200;
        imageOptions.fallback = R.mipmap.no_product_small;
    }

    public void setImage(Uri uri) {
        holder.imgFlower.setImageURI(uri);
    }

    @Override
    public TextViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_grid, parent, false);

        holder = new TextViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final TextViewHolder holder, final int position) {

        holder.relativeUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePhotoFromCamera();
                Const.imgPosition = position;
            }
        });
        Picasso.with(context).load(arlist.get(position).image).into(holder.imgFlower);
        holder.ivDate.setText(arlist.get(position).ChangeOverDate);
        holder.imgFlower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (context instanceof UpdateStatusActivity) {
                    ((UpdateStatusActivity) context).showPopupImgHistory(((UpdateStatusActivity) context).tvAddress, arlist.get(position).image, arlist.get(position).customer_name, arlist.get(position).customer_note);
                } else {
                    ((HistoryActivity) context).showPopupImgHistory(((HistoryActivity) context).tvName, arlist.get(position).image, arlist.get(position).customer_name, arlist.get(position).customer_note);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return arlist.size();
    }

    public void takePhotoFromCamera() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((UpdateStatusActivity) context, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, Const.PERMISSION_CAMERA_REQUEST_CODE);
        } else {
            Calendar cal = Calendar.getInstance();
            File file = new File(Environment.getExternalStorageDirectory(),
                    (cal.getTimeInMillis() + ".jpg"));

            if (!file.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {

                file.delete();
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Const.cameraURI = Uri.fromFile(file);
            Intent cameraIntent = new Intent(
                    MediaStore.ACTION_IMAGE_CAPTURE);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Const.cameraURI);
            ((UpdateStatusActivity) context).startActivityForResult(cameraIntent, Const.TAKE_PHOTO);
        }
    }

    public class TextViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgUpload, imgFlower;
        private RelativeLayout relativeUpload;
        private TextView ivDate;

        public TextViewHolder(View view) {
            super(view);

            imgUpload = (ImageView) view.findViewById(R.id.imgUpload);
            imgFlower = (ImageView) view.findViewById(R.id.imgFlower);
            ivDate = (TextView) view.findViewById(R.id.ivDate);
            relativeUpload = (RelativeLayout) view.findViewById(R.id.relativeUpload);
            if (context instanceof HistoryActivity) {
                relativeUpload.setVisibility(View.GONE);
                ivDate.setVisibility(View.VISIBLE);
            } else {
                relativeUpload.setVisibility(View.VISIBLE);
                ivDate.setVisibility(View.GONE);
            }
            final int sdk = android.os.Build.VERSION.SDK_INT;
            if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                imgFlower.setBackgroundDrawable(context.getResources().getDrawable(R.mipmap.no_product_small));
            } else {
                imgFlower.setBackgroundResource(R.mipmap.no_product_small);
            }
        }
    }
}


