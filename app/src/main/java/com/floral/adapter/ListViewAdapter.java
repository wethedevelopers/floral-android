package com.floral.adapter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.floral.DetailActivity;
import com.floral.HistoryActivity;
import com.floral.R;
import com.floral.fragment.ListViewFragment;
import com.floral.location.LocationHelper;
import com.floral.model.Customer;
import com.floral.utils.AndyUtils;
import com.floral.utils.Const;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

public class ListViewAdapter extends RecyclerView.Adapter<ListViewAdapter.TextViewHolder>
        implements LocationHelper.OnLocationReceived {
    private List<Customer> list;
    private Context context;
    private LocationHelper locationHelper;
    private ListViewFragment fragment;
    private BroadcastReceiver GpsChangeReceiver;
    private Location myLocation;

    public ListViewAdapter(Context context, List<Customer> list, LocationHelper locationHelper, ListViewFragment fragment, BroadcastReceiver GpsChangeReceiver) {
        this.context = context;
        this.list = list;
        this.locationHelper = locationHelper;
        this.fragment = fragment;
        this.GpsChangeReceiver = GpsChangeReceiver;
    }

    @Override
    public TextViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_view, parent, false);
        return new TextViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TextViewHolder holder, final int position) {

        holder.tvPlaceName.setText(list.get(position).getCustomerName());
        holder.tvAddress.setText(list.get(position).getAddress());


        holder.tvHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, HistoryActivity.class);
                intent.putExtra("data", list.get(position));
                intent.putExtra("customer_name", list.get(position).getCustomerName());
                context.startActivity(intent);
            }
        });
        holder.tvNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Const.DESTINATION_LAT = Double.parseDouble(list.get(position).getLatitude());
                Const.DESTINATION_LONG = Double.parseDouble(list.get(position).getLongitude());

                LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
                if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    fragment.showLocationOffDialog();
                } else {
                    fragment.removeLocationOffDialog();
                }

                context.registerReceiver(GpsChangeReceiver, new IntentFilter(
                        LocationManager.PROVIDERS_CHANGED_ACTION));

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    fragment.checkLocationPermission();
                } else {
                    String url = Const.DIRECTION_API + "saddr=" + String.valueOf(myLocation.getLatitude())
                            + "," + String.valueOf(myLocation.getLongitude()) + "&"
                            + "daddr=" +
                            Const.DESTINATION_LAT + "," + Const.DESTINATION_LONG;
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                }
            }
        });

        holder.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);

            }
        });
        holder.tvPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("data", list.get(position));
                context.startActivity(intent);

            }
        });
        if (list.get(position).getChangeOverPinStatus().equals("red")) {
            holder.tvPin.setImageResource(R.drawable.location_pink);

        } else if (list.get(position).getChangeOverPinStatus().equals("green")) {
            holder.tvPin.setImageResource(R.drawable.location_green);
        } else {
            holder.tvPin.setImageResource(R.drawable.location_yellow);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onLocationReceived(Location location) {
        if (location != null)
            myLocation = location;
    }

    @Override
    public void onLocationReceived(LatLng latlong) {

    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnected(Location location) {
        AndyUtils.removeCustomProgressDialog();
        if (location != null) {
            myLocation = location;
            locationHelper = new LocationHelper(context);
            locationHelper.setLocationReceivedLister(this);
            locationHelper.onStart();
            String url = Const.DIRECTION_API + "saddr=" + String.valueOf(myLocation.getLatitude())
                    + "," + String.valueOf(myLocation.getLongitude()) + "&"
                    + "daddr=" +
                    "90.5641" + "," + "76.05545";
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(browserIntent);
        } else {
        }
    }

    public class TextViewHolder extends RecyclerView.ViewHolder {

        private TextView tvPlaceName, tvAddress, tvHistory, tvNavigation;
        private ImageView tvPin;

        private LinearLayout llMain;


        public TextViewHolder(View view) {
            super(view);

            llMain = (LinearLayout) view.findViewById(R.id.llMain);
            tvPlaceName = (TextView) view.findViewById(R.id.tvPlaceName);
            tvAddress = (TextView) view.findViewById(R.id.tvAddress);
            tvHistory = (TextView) view.findViewById(R.id.tvHistory);
            tvNavigation = (TextView) view.findViewById(R.id.tvNavigation);
            tvPin = (ImageView) view.findViewById(R.id.tvPin);


        }
    }

}

