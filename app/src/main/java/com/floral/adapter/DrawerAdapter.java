package com.floral.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RadioButton;

import com.floral.R;
import com.floral.model.Zone;
import com.floral.utils.Const;

import java.util.ArrayList;

/**
 * Created by bhagvati on 28/1/17.
 */
public class DrawerAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<Zone> menuItems;

    int selected_position;


    public DrawerAdapter(Context context, ArrayList<Zone> menuItems, int selected_position)
    {
        this.context = context;
        this.menuItems = menuItems;
        this.selected_position = selected_position;
        inflater = LayoutInflater.from(context);

    }

    public int getCount() {
        return menuItems.size();
    }

    public Object getItem(int position) {
        return menuItems.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public View getView(final int position, View convertView, ViewGroup parent) {
       final ViewHolder holder;
        if (convertView == null) {
            convertView = this.inflater.inflate(R.layout.drawer_item, parent, false);
            holder = new ViewHolder();
            holder.tvMenuText = (RadioButton) convertView.findViewById(R.id.tvMenuText);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }


        if(selected_position == position)
        {
            holder.tvMenuText.setChecked(true);
        }else
        {

            holder.tvMenuText.setChecked(false);
           /* if (this.menuItems.get(position).isSelected())
            {
                selected_position=position;
                holder.tvMenuText.setChecked(true);
            }else {
                holder.tvMenuText.setChecked(false);
            }*/
        }
       /* if (this.menuItems.get(position).isSelected())
        {
            selected_position=position;
            holder.tvMenuText.setChecked(true);
        }else {
            holder.tvMenuText.setChecked(false);
        }*/

        holder.tvMenuText.setText(this.menuItems.get(position).getZoneName());
        holder.tvMenuText.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {


                if (((RadioButton) view).isChecked())
                {
                    selected_position= position;

                    Const.ZONE_ID = menuItems.get(position).getID();
                    Const.ZONE_NAME =  menuItems.get(position).getZoneName();
                    Log.d("------->CCCCC  ", Const.ZONE_NAME);
                    Log.d("get check ::: ",holder.tvMenuText.isChecked()+"");


                }
                else
                {
                    selected_position=-1;

                    Log.d("selected pos else :: ",""+selected_position +" "+ position);
                }
                notifyDataSetChanged();
            }
        });




       /* if (this.menuItems.get(position).isSelected()) {

            holder.tvMenuText.setChecked(true);
            Const.ZONE_ID = this.menuItems.get(position).getID();
            Const.ZONE_NAME =  menuItems.get(position).getZoneName();
            selected_position =position;


        } else {

            holder.tvMenuText.setChecked(false);
            selected_position=-1;
        }*/




        /*holder.tvMenuText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if(b) {

                    Const.ZONE_ID = menuItems.get(position).getID();
                    Const.ZONE_NAME =  menuItems.get(position).getZoneName();

                    holder.tvMenuText.setChecked(true);

                    Log.d("------->CCCCC  ", Const.ZONE_NAME);
                   // ((DeliveryRouteActivity)context).changeStateCheck(position);

                }else
                {
                    holder.tvMenuText.setChecked(false);
                }
            }
        });

        if (this.menuItems.get(position).isSelected()) {

            holder.tvMenuText.setChecked(true);
            Const.ZONE_ID = this.menuItems.get(position).getID();
            Const.ZONE_NAME =  menuItems.get(position).getZoneName();



        } else {

            holder.tvMenuText.setChecked(false);
        }
*/

        return convertView;
    }

    private class ViewHolder {
        RadioButton tvMenuText;
    }
}
