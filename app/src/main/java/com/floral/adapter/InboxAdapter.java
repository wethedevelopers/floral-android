package com.floral.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.floral.R;
import com.floral.model.Inbox;

import java.util.List;

/**
 * Created by bhagvati on 9/2/17.
 */
public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.TextViewHolder> {

    private List<Inbox> list;
    private Context context;

    public InboxAdapter(Context context, List<Inbox> list) {

        this.context = context;
        this.list = list;

    }

    @Override
    public TextViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_inbox, parent, false);
        return new TextViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final TextViewHolder holder, final int position)
    {


        holder.tvTime.setText(list.get(position).getSentOn());
        holder.tvMsgFrom.setText(context.getResources().getString(R.string.from)+" "+list.get(position).getSender());
        holder.tvMsg.setText(list.get(position).getMessage());



    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class TextViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTime, tvMsgFrom, tvMsg;



        public TextViewHolder(View view) {
            super(view);


            tvTime = (TextView) view.findViewById(R.id.tvTime);
            tvMsgFrom = (TextView) view.findViewById(R.id.tvMsgFrom);
            tvMsg = (TextView) view.findViewById(R.id.tvMsg);


        }
    }



}
