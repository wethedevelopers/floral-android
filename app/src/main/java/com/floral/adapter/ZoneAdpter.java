package com.floral.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.floral.R;
import com.floral.model.Zone;

import java.util.ArrayList;

/**
 * Created by bhagvati on 30/1/17.
 */
public class ZoneAdpter  extends BaseAdapter {
    public LayoutInflater inflater;
    public ArrayList<Zone> list;
    public Context con;

    public ZoneAdpter(Activity context, ArrayList<Zone> musiclist) {

        con = context;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        list = musiclist;
    }

    public int getCount() {
        return list.size();
    }

    public Zone getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertview, ViewGroup parent) {

        ViewHolder holder;
        if (convertview == null) {
            holder = new ViewHolder();
            convertview = inflater.inflate(R.layout.row_zone, null);
            holder.tvZoneName = (TextView) convertview.findViewById(R.id.tvZoneName);
            convertview.setTag(holder);
        } else {
            holder = (ViewHolder) convertview.getTag();
        }

        holder.tvZoneName.setText(list.get(position).getZoneName());

        return convertview;
    }

    public class ViewHolder {
        TextView tvZoneName;
    }

}
