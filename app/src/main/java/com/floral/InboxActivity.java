package com.floral;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.floral.adapter.InboxAdapter;
import com.floral.model.Inbox;
import com.floral.parse.HttpRequester;
import com.floral.parse.ParseContent;
import com.floral.utils.AndyUtils;
import com.floral.utils.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class InboxActivity extends BaseActivity {


    private RecyclerView recyclerView;
    private InboxAdapter adapter;
    private ArrayList<Inbox> list;
    private TextView tvUserName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        inflater.inflate(R.layout.activity_inbox, flContainer);

        final GestureDetector mGestureDetector = new GestureDetector(InboxActivity.this, new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });


        llDeliveryRoute.setSelected(false);
        llDeliveryRoute.setEnabled(true);
        llInbox.setSelected(true);
        llChangePwd.setSelected(false);
        llLogout.setSelected(false);

        list = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        //adapter = new ListViewAdapter(activity, ((DeliveryRouteActivity) activity).listRoute);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(InboxActivity.this);
        gridLayoutManager.supportsPredictiveItemAnimations();
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.getRecycledViewPool().clear();



        recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {


            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());


                if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                    // Toast.makeText(activity,"The Item Clicked is: "+recyclerView.getChildPosition(child),Toast.LENGTH_SHORT).show();

                    int pos = recyclerView.getChildPosition(child);


                        Intent intent = new Intent(InboxActivity.this, InboxDetailActivity.class);
                        intent.putExtra("desc", list.get(pos).getMessage());
                        startActivity(intent);



                    return true;

                }


                return false;
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }
        });

        tvUserName = (TextView)findViewById(R.id.tvUserName);
        tvUserName.setText(preferenceHelper.getName());

        getInbox();

    }

    private void getInbox()
    {

        if (!AndyUtils.isNetworkAvailable(InboxActivity.this)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    InboxActivity.this);
            return;
        }

        AndyUtils.showSimpleProgressDialog(InboxActivity.this);
        HashMap<String, String> map = new HashMap<>();
        map.put(Const.URL, Const.ServiceType.INBOX);

        map.put("id", preferenceHelper.getUserId());

        new HttpRequester(InboxActivity.this, map, Const.ServiceCode.INBOX, false, this);
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        ParseContent parseContent = new ParseContent(InboxActivity.this);
        AndyUtils.removeSimpleProgressDialog();

        switch (serviceCode) {
            case Const.ServiceCode.INBOX:
                if (parseContent.isStatusOk(response)) {

                    list =parseContent.getInbox(response);

                    adapter = new InboxAdapter(InboxActivity.this, list);
                    recyclerView.getRecycledViewPool().clear();
                    recyclerView.setAdapter(adapter);

                }


                    break;
        }
    }

}
