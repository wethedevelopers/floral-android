package com.floral.utils;

import android.net.Uri;

import com.floral.model.Zone;

import java.util.ArrayList;

public class Const {
    public static final String DEVICE_TYPE = "android";
    public static final String ERROR_CODE_PREFIX = "error_";
    public static final String PREF_NAME = "MIID";
    public static final String TAG = "FLORAL";
    public static final String URL = "url";

    //customer type
    public static final String DIRECTION_API_KEY = "AIzaSyB6YhFXMC7hchSrfm-nQDFRA6BQWbGwymM";
    //Request Permission Code
    public static final int PERMISSION_LOCATION_REQUEST_CODE = 211;
    public static final int PERMISSION_STORAGE_REQUEST_CODE = 212;
    public static final int PERMISSION_CAMERA_REQUEST_CODE = 213;
    //    Fragment Tags
    public static final String FRAGMENT_MAP = "fragment_map";
    public static final String FRAGMENT_LIST = "fragment_list";
    public static final String DIRECTION_API = "https://maps.google.com/maps?";
    public static final String GEO_CODE_API = "http://maps.googleapis.com/maps/api/geocode/json?address=";
    public static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/autocomplete/json";
    public static final String BROWSER_KEY = "AIzaSyBdp_puHye3BhtuirjY74CP1DLu46uiIQE";
    //permission constants.
    public static final int PERMISSION_LOCATION = 1;
    public static final int PERMISSION_CALL = 2;
    public static final int PERMISSION_CAMERA = 3;
    public static final int PERMISSION_GALLLERY = 4;
    public static final int TAKE_PHOTO = 5;
    public static final int CHOOSE_PHOTO = 6;
    public static final int PERMISSION_GET_ACCOUNT_REQUEST_CODE = 7;
    public static final int PICK_CONTACT = 8;
    public static final int PERMISSION_PICK_CONTACT = 9;
    // Placesurls
    public static final String PLACES_API_BASE1 = "https://maps.googleapis.com/maps/api/place";
    public static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    public static final String TYPE_NEAR_BY = "/nearbysearch";
    public static final String OUT_JSON = "/json";


//    public static final String BROWSER_KEY = "AIzaSyCLJYVhiTO1pMhWpe2IpwmR0fcWDazXfxI"
//    public static final String BROWSER_KEY = "AIzaSyD5YMDzxaYBEqcwoBL1vZEAlYN6cB15nJA";
    public static final String PLACES_AUTOCOMPLETE_API_KEY = "AIzaSyBpcfE5gbXYRTAjXf7RAor0JdPQfKXm-z4";
    public static String FILTER_TYPE = "all";
    public static String ALL = "all";
    public static String GREEN = "green";
    public static String YELLOW = "yellow";
    public static String RED = "red";
    public static String ZONE_ID = "";
    public static String ZONE_NAME = "";
    public static String ADDRESS = "";
    public static Uri cameraURI;
    public static int imgPosition = 0;
    public static ArrayList<Zone> ZONE_LIST = new ArrayList<>();
    public static double DESTINATION_LAT = 0.0;
    public static double DESTINATION_LONG = 0.0;

    public class ServiceType {
        //        static final String HOST_URL = "http://192.168.0.197/big_red/public/";
        public static final String HOST_URL = "https://firoutemap.com/siteadmin/api/";
        // public static final String BASE_URL = HOST_URL + "user/";
        public static final String login = HOST_URL + "api_login.php";
        public static final String ZONE_LIST = HOST_URL + "api_zoneList.php";
        public static final String LOGOUT = HOST_URL + "api_logout_remark.php";
        public static final String GET_CUSTOMER = HOST_URL + "api_customerList1.2.php";
        public static final String SEARCH = HOST_URL + "api_customerList1.2.php";
        public static final String ADVANCE_SEARCH = HOST_URL + "api_customerList1.2.php";
        public static final String CHANGE_OVER_REMARK_YES = HOST_URL + "api_changeOver_remarkyes_1.1.php";
        public static final String CHANGE_OVER_IMGE = HOST_URL + "api_changeOver1.1.php";
        //public static final String HISTORY= HOST_URL + "api_history.php";
        public static final String HISTORY = HOST_URL + "api_history_new.php";
        public static final String CHANGE_PWD = HOST_URL + "api_changePassword.php";
        public static final String INBOX = HOST_URL + "api_messages.php";


    }

    public class ServiceCode {
        public static final int LOGIN = 1;
        public static final int ZONE_LIST = 2;
        public static final int LOGOUT = 3;
        public static final int GET_CUSTOMER = 4;
        public static final int SEARCH = 5;
        public static final int CHANGE_OVER_REMARK_YES = 6;
        public static final int CHANGE_OVER_IMGE = 7;
        public static final int HISTORY = 8;
        public static final int DRAW_PATH_ROAD = 9;
        public static final int CHANGE_PWD = 10;
        public static final int INBOX = 11;
        public static final int DRAW_PATH_CLIENT = 25;
        public static final int GET_CUSTOMER_DUPLICATE = 26;
    }

    public class Params {
        public static final String ID = "ID";
        public static final String PICTURE = "picture";
        public static final String EMAIL = "Email";
        public static final String PASSWORD = "Password";
        public static final String NAME = "DeliveryBoyName";
        public static final String PHONE = "Phone";

    }
}
