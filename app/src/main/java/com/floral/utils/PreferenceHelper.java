package com.floral.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {
    private SharedPreferences app_prefs;
    private final String USER_ID = "user_id";
    private final String EMAIL = "email";
    private final String PASSWORD = "password";
    private final String DEVICE_TOKEN = "device_token";
    private final String SESSION_TOKEN = "session_token";
    private final String PICTURE = "picture";
    private final String PHONE = "phone";
    private final String NAME = "name";
    private final String HOME_ADDRESS="home_address";
    private final String ADDRESS="address";


    public PreferenceHelper(Context context) {
        app_prefs = context.getSharedPreferences(Const.PREF_NAME, Context.MODE_PRIVATE);
    }


    public void putAddress(String userId) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(ADDRESS, userId);
        edit.apply();
    }

    public String getAddress() {
        return app_prefs.getString(ADDRESS, null);
    }


    public void putHomeAddress(String userId) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(HOME_ADDRESS, userId);
        edit.apply();
    }

    public String getHomeAddress() {
        return app_prefs.getString(HOME_ADDRESS, null);
    }

    public void putUserId(String userId) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(USER_ID, userId);
        edit.apply();
    }

    public String getUserId() {
        return app_prefs.getString(USER_ID, null);
    }



    public void putName(String name) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(NAME, name);
        edit.apply();
    }

    public String getName() {
        return app_prefs.getString(NAME, null);
    }

    public void putEmail(String email) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(EMAIL, email);
        edit.apply();
    }

    public String getEmail() {
        return app_prefs.getString(EMAIL, null);
    }

    public void putPassword(String password) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(PASSWORD, password);
        edit.apply();
    }

    public String getPassword() {
        return app_prefs.getString(PASSWORD, null);
    }

    public void putSessionToken(String sessionToken) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(SESSION_TOKEN, sessionToken);
        edit.apply();
    }

    public String getSessionToken() {
        return app_prefs.getString(SESSION_TOKEN, null);
    }

    public void putPicture(String picture) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(PICTURE, picture);
        edit.apply();
    }

    public String getPicture() {
        return app_prefs.getString(PICTURE, "");
    }

    public void putPhoneNum(String phoneNum) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(PHONE, phoneNum);
        edit.apply();
    }

    public String getPhoneNum() {
        return app_prefs.getString(PHONE, null);
    }

    public void putDeviceToken(String deviceToken) {
        SharedPreferences.Editor edit = app_prefs.edit();
        edit.putString(DEVICE_TOKEN, deviceToken);
        edit.apply();
    }

    public String getDeviceToken() {
        return app_prefs.getString(DEVICE_TOKEN, null);
    }

    public void logout(){
        putUserId(null);
        putSessionToken(null);
        putEmail(null);
        putPhoneNum(null);
        putName(null);
        //        app_prefs.edit().clear().apply();
    }


}
