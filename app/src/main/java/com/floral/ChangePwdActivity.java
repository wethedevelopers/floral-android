package com.floral;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.floral.parse.HttpRequester;
import com.floral.parse.ParseContent;
import com.floral.utils.AndyUtils;
import com.floral.utils.Const;

import java.util.HashMap;

public class ChangePwdActivity extends BaseActivity {

    private TextView tvChangePwd,tvUserName;
    private EditText etPassword,etConfirmPwd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        inflater.inflate(R.layout.activity_change_pwd, flContainer);

        initUI();

    }

    private void initUI()
    {

        llDeliveryRoute.setSelected(false);
        llDeliveryRoute.setEnabled(true);
        llInbox.setSelected(false);
        llChangePwd.setSelected(true);
        llLogout.setSelected(false);


        etPassword = (EditText) findViewById(R.id.etPassword);
        etConfirmPwd = (EditText) findViewById(R.id.etConfirmPwd);
        tvUserName=(TextView)findViewById(R.id.tvUserName);
        tvUserName.setText(preferenceHelper.getName());


        tvChangePwd = (TextView) findViewById(R.id.tvChangePwd);
        tvChangePwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(isValidate())
                {
                    changePwd();
                }


            }
        });

    }

    private void changePwd()
    {
        if (!AndyUtils.isNetworkAvailable(ChangePwdActivity.this)) {
            AndyUtils.showToast(getResources().getString(R.string.no_internet),
                    ChangePwdActivity.this);
            return;
        }

        if (isValidate()) {
            AndyUtils.showSimpleProgressDialog(ChangePwdActivity.this);
            HashMap<String, String> map = new HashMap<>();
            map.put(Const.URL, Const.ServiceType.CHANGE_PWD);
            map.put("id", preferenceHelper.getUserId());
            map.put("Password", etPassword.getText().toString());

            new HttpRequester(ChangePwdActivity.this, map, Const.ServiceCode.CHANGE_PWD, false, this);
        }
    }

    @Override
    public void onTaskCompleted(String response, int serviceCode) {
        ParseContent parseContent = new ParseContent(ChangePwdActivity.this);
        AndyUtils.removeSimpleProgressDialog();

        switch (serviceCode) {
            case Const.ServiceCode.CHANGE_PWD:
                if (parseContent.isStatusOkWithToast(response)) {



                    etPassword.setText("");
                    etConfirmPwd.setText("");

                }
        }
    }

    public boolean isValidate() {
        String msg = null;
        if (TextUtils.isEmpty(etPassword.getText().toString())
                && TextUtils.isEmpty(etPassword.getText().toString())) {
            msg = getResources().getString(R.string.text_enter_password);
        }
        else if (TextUtils.isEmpty(etConfirmPwd.getText().toString()))
        {
            msg = getResources().getString(R.string.text_change_pwd);
        }else if(!etPassword.getText().toString().equals(etConfirmPwd.getText().toString()))
        {

            msg = getResources().getString(R.string.text_confirm);
        }



        if (msg == null)
            return true;

        Toast.makeText(ChangePwdActivity.this, msg, Toast.LENGTH_SHORT).show();
        return false;
    }

}
